angular.module('starter.controllers', [])

/*  InicioCtrl */
.controller('InicioCtrl', function($scope, $stateParams, $state, $http, $ionicLoading) {
    $scope.loginError = false;


    /* function doLogin() */
    $scope.doLogin = function() {

        $ionicLoading.show({template: '<center>Conectant...<br/> <ion-spinner class="spinner-light" icon="spiral"></ion-spinner></center>'});

        if ( angular.isDefined($scope.login.username) && angular.isDefined($scope.login.password) ) {

            $http.post(
                'http://nuk.isdeveloping.com/laiflyadema/backoffice/api/users/login',
                {
                    username: $scope.login.username,
                    password: $scope.login.password
                }).
                then(function(response) {

                  console.log(response);

                    $ionicLoading.hide();

                    if ( response.status == 200 ) {
                        // TO-DO: retornar auth para guardar en local.Storage
                        if (response.data.status == 200 ) {
                            $scope.loginError = false;
                            window.localStorage['app.User'] = JSON.stringify(response.data.user);

                            $state.go("app.benvinguts");
                        }
                        else {
                            $scope.loginError = true;
                        }
                    }
                },
                function(response) { // error
                    $scope.loginError = true;
                    $ionicLoading.hide();
                }
            );
        }
        else
            $scope.loginError = true;
    }

    $scope.doLogout = function() {
      window.localStorage.clear();
    }


})
/*  end InicioCtrl */


/*  AppCtrl */
.controller('AppCtrl', function($scope, $ionicModal, $timeout, $http) {
    var user = window.localStorage['app.User'];
    $scope.user = JSON.parse(user);

    // TO-DO
    // Obtenemos los Modulos/Secciones
    // en las que el Local puede tomar mediciones
    // $http.get('http://nuk.isdeveloping.com/laiflyadema/backoffice/api/module_local/'+user.local_id+'.json').
    //     then(function(response) {
    //          if ( response.status == 200 ) {
    //             window.localStorage['app.Modules'] = JSON.stringify(response.data);
    //         }
    //     },
    //     function(response) {
    //     }
    // );

    // Obtenemos los Proveedores que pertenecen al Local
    $http.get('http://nuk.isdeveloping.com/laiflyadema/backoffice/api/provider/local/'+$scope.user.local_id+'.json').
        then(function(response) {
            if ( response.status == 200 ) {
                window.localStorage['app.Providers'] = JSON.stringify(response.data.provider);
            }
        },
        function(response) {
        }
    );

    $http.get('http://nuk.isdeveloping.com/laiflyadema/backoffice/api/local/'+$scope.user.local_id+'.json').
        then(function(response) {
            if ( response.status == 200 ) {
                window.localStorage['app.LocalConfig'] = JSON.stringify(response.data.local);
            }
        },
        function(response) {
        }
    );

    // Obtenemos los Equipos que pertenecen al Local
    $http.get('http://nuk.isdeveloping.com/laiflyadema/backoffice/api/equipo/local/'+$scope.user.local_id+'.json').
        then(function(response) {
             if ( response.status == 200 ) {
                window.localStorage['app.Equipos'] = JSON.stringify(response.data.equipo);
            }
        },
        function(response) {
        }
    );

    // Obtenemos los Equipos que pertenecen al Local
    $http.get('http://nuk.isdeveloping.com/laiflyadema/backoffice/api/zona/local/'+$scope.user.local_id+'.json').
        then(function(response) {
             if ( response.status == 200 ) {
                window.localStorage['app.Zonas'] = JSON.stringify(response.data.zona);
            }
        },
        function(response) {
        }
    );


    // Obtenemos las ultimas Mediciones
    $http.get('http://nuk.isdeveloping.com/laiflyadema/backoffice/api/medicion/all/'+$scope.user.local_id+'.json').
        then(function(response) {
             if ( response.status == 200 ) {
                window.localStorage['app.MedicionesListado'] = JSON.stringify(response.data.mediciones);
            }
        },
        function(response) {
        }
    );

    // Obtenemos las ultimas Mediciones
    /*$http.get('http://nuk.isdeveloping.com/laiflyadema/backoffice/api/local/user/'+$scope.user.id+'.json').
        then(function(response) {
             if ( response.status == 200 ) {
                window.localStorage['app.Milocal'] = JSON.stringify(response.data.local);
            }
        },
        function(response) {
        }
    );*/

})
/* end AppCtrl */


/*  NovamedicioCtrl. Afegir una medició */
.controller('NovamedicioCtrl', function($scope, $state) {

    $scope.afegeix = function(tipus){
        $state.go('app.medicio', { laid : tipus } );
    }


    configura = [];
    rawconfig= JSON.parse(window.localStorage['app.LocalConfig']);
    configura[1]=rawconfig['medicio1'];
    configura[2]=rawconfig['medicio2'];
    configura[3]=rawconfig['medicio3'];
    configura[4]=rawconfig['medicio4'];
    configura[5]=rawconfig['medicio5'];
    configura[6]=rawconfig['medicio6'];
    configura[7]=rawconfig['medicio7'];
    configura[8]=rawconfig['medicio8'];
    configura[9]=rawconfig['medicio9'];
    configura[10]=rawconfig['medicio10'];
    $scope.config=configura;




})
/*  end NovamedicioCtrl. Afegir una medició*/

.controller('MedicioCtrl', function($scope, $stateParams, $state, $http, $ionicScrollDelegate,$ionicLoading,$timeout) {

    var providers = window.localStorage['app.Providers'];

    $ionicLoading.show({template: '<center>Carregant dades...<br/> <ion-spinner class="spinner-light" icon="spiral"></ion-spinner></center>'});

    $http.get('http://nuk.isdeveloping.com/laiflyadema/backoffice/api/provider/local/'+$scope.user.local_id+'.json').
        then(function(response) {
            if ( response.status == 200 ) {
                if(window.localStorage['app.Providers']!=JSON.stringify(response.data.provider)){
                  window.localStorage['app.Providers'] = JSON.stringify(response.data.provider);
                  $state.go($state.current, {}, {reload: true});
                }
            }
            $ionicLoading.hide();
        },
        function(response) {
          $ionicLoading.hide();
        }
    );

    // Obtenemos los Equipos que pertenecen al Local
    var equipos = window.localStorage['app.Equipos'];
    $http.get('http://nuk.isdeveloping.com/laiflyadema/backoffice/api/equipo/local/'+$scope.user.local_id+'.json').
        then(function(response) {
            if ( response.status == 200 ) {
                if(window.localStorage['app.Equipos']!=JSON.stringify(response.data.equipo)){
                  window.localStorage['app.Equipos'] = JSON.stringify(response.data.equipo);
                  $state.go($state.current, {}, {reload: true});
                }
            }
        },
        function(response) {
        }
    );

    // comprobamos si existen Providers
    if ( providers.length > 0 ) {
        $scope.providers = JSON.parse(providers);

        // cast schema database a default array
        for (i=0; i<$scope.providers.length; i++) {
            $scope.providers[i].tecarn      = $scope.providers[i].carne;
            $scope.providers[i].tepeix      = $scope.providers[i].pescado;
            $scope.providers[i].tecongelats = $scope.providers[i].congelados;
            $scope.providers[i].tefruita    = $scope.providers[i].fruta;
        }

        $scope.proveidors = $scope.providers;

        /* evento onChange select Proveedor */
        $scope.onChangeProvider = function() {
          console.log($scope.providers);
            for(i=0; i<$scope.providers.length; i++) {
                if($scope.providers[i].id == $scope.medicio.proveidor){
                    $scope.nomproveidor=$scope.providers[i].name;
                    console.log($scope.nomproveidor);
                    if ($scope.providers[i].carne == true)      { $scope.medicio.tecarn=true;      } else { $scope.medicio.tecarn=false; }
                    if ($scope.providers[i].pescado == true)    { $scope.medicio.tepeix=true;      } else { $scope.medicio.tepeix=false; }
                    if ($scope.providers[i].congelados == true) { $scope.medicio.tecongelats=true; } else { $scope.medicio.tecongelats=false; }
                    if ($scope.providers[i].fruta == true)      { $scope.medicio.tefruita=true;    } else { $scope.medicio.tefruita=false; }
                }
            }
        }
        /* evento onChange select Proveedor */
    }





    // comprobamos si existen Equipos de refrigeracion
    if ( equipos.length > 0 ){
        $scope.equipos = JSON.parse(equipos);

        // cast schema database a default array
        for (i=0; i<$scope.equipos.length; i++) {
            $scope.equipos[i].nom    = $scope.equipos[i].name;
            $scope.equipos[i].maxima = $scope.equipos[i].max;
            $scope.equipos[i].minima = $scope.equipos[i].min;
            $scope.equipos[i].bona   = $scope.equipos[i].standard;
        }

        $scope.temps = $scope.equipos;
    }

    var zonas ='';

    $http.get('http://nuk.isdeveloping.com/laiflyadema/backoffice/api/zona/local/'+$scope.user.local_id+'.json').
        then(function(response) {
             if ( response.status == 200 ) {
                var zonas =window.localStorage['app.Zonas'] = JSON.stringify(response.data.zona);

                // comprobamos si existen Equipos de refrigeracion
                if ( zonas.length > 0 ){
                    $scope.zonas = JSON.parse(zonas);

                    // cast schema database a default array
                    for (i=0; i<$scope.zonas.length; i++) {
                        $scope.zonas[i].nom    = $scope.zonas[i].name;
                    }

                    $scope.zones = $scope.zonas;

                }
            }else{
                var zonas = window.localStorage['app.Zonas'];
                // comprobamos si existen Equipos de refrigeracion
                if ( zonas.length > 0 ){
                    $scope.zonas = JSON.parse(zonas);

                    // cast schema database a default array
                    for (i=0; i<$scope.zonas.length; i++) {
                        $scope.zonas[i].nom    = $scope.zonas[i].name;
                    }

                    $scope.zones = $scope.zonas;
                }
            }
        },
        function(response) {
        }
    );




  // RETRIEVE AND SET ID
  laid=$stateParams.laid;
  $scope.laid=laid;

  // CALCULO DE FECHAS
  var weekday = new Array(7);
  weekday[0]=  "Diumenge";
  weekday[1] = "Dilluns";
  weekday[2] = "Dimarts";
  weekday[3] = "Dimecres";
  weekday[4] = "Dijous";
  weekday[5] = "Divendres";
  weekday[6] = "Dissabte";

  var months = new Array(12);
  months[0]=  "Gener";
  months[1] = "Febrer";
  months[2] = "Març";
  months[3] = "Abril";
  months[4] = "Maig";
  months[5] = "Juny";
  months[6] = "Juliol";
  months[7] = "Agost";
  months[8] = "Setembre";
  months[9] = "Octubre";
  months[10] = "Novembre";
  months[11] = "Desembre";

  var d = new Date();

  // SET DE FECHAS
  $scope.diasetmana=weekday[d.getDay()];
  $scope.mes=months[d.getMonth()];
  $scope.dia=d.getDate();
  $scope.fecha=d.getDate()+'-'+d.getMonth()+'-'+d.getFullYear();

  //SET DE ERRORES
  $scope.alertas = [];


  // SET DE MENSAJES (el html queda más limpio...)
  $scope.errorComu="Si us plau, repetiu la medició abans d'enviarla per assegurar que no es tracta d'un error de medició.";

  // SET DE VARIABLES SEGUN ID
  if(laid==1){

    // SET DE TITULO
    $scope.title="CLOR RESIDUAL AIGUA DE CONSUM";

    // SET DE VARIABLES AUXILIARES DE MEDICION
    $scope.clor = {};
    $scope.medicio = {};
    $scope.medicio.valor=20;
    $scope.clor_max=100;
    $scope.clor_min=20;
    $scope.clor.extensio="ppm";

  }else if(laid==2){

    // SET DE TITULO
    $scope.title="NETEJA I DESINFECCIÓ DE LA FRUITA I VERDURA DE CONSUM EN CRU";

    // SET DE VARIABLES AUXILIARES DE MEDICION
    $scope.clor = {};
    $scope.medicio = {};
    $scope.medicio.valor=90;
    $scope.clor_max=100;
    $scope.clor_min=70;
    $scope.clor.extensio="ppm";

  }else if(laid==3){

    // SET DE TITULO
    $scope.title="TEMPERATURES DELS EQUIPS DE FRED";

    $scope.extensio='ºC';
    $scope.medicio = {};
    $scope.medicio.temp = [];

    for(a=0; a<$scope.temps.length; a++){
      $scope.medicio.temp[a]={};
      $scope.medicio.temp[a].valor=$scope.temps[a].bona;

    }

    $scope.valor=2;
    $scope.maxim=3;
    $scope.minim=0;

  }else if(laid==4){

    $scope.title="CRITERIS EMMAGATZEMATGE ALIMENTS DINS EQUIPS DE FRED";
    $scope.medicio = {};
    $scope.medicio.limits=false;
    $scope.medicio.senseproteccio=false;
    $scope.medicio.preparats=false;
    $scope.medicio.etiqueta=false;
    $scope.medicio.descon=false;
    $scope.medicio.dates=false;
    $scope.medicio.sensedata=false;
    $scope.medicio.cambrasensedata=0;

  }else if(laid==5){

    $scope.title="OLI DE LES FREGIDORES";
    $scope.medicio = {};
    $scope.medicio.oleotest=0;

  }else if(laid==6){

    $scope.title="TEMPERATURA D'ESBANDIT EQUIPS DE FRED";
    $scope.medicio = {};
    $scope.medicio.esbandit=85;

  }else if(laid==7){

    $scope.title="RECEPCIÓ MERCADERIA";

    // model general
    $scope.medicio={};

    $scope.medicio.proveidor=0;

    // variables del que porta el proveidor
    $scope.medicio.tecongelats=true;
    $scope.medicio.tecarn=true;
    $scope.medicio.tepeix=true;
    $scope.medicio.tefruita=true;

    // medicions
    $scope.medicio.andorra=true;
    $scope.medicio.correspon=true;
    $scope.medicio.documentacio=true;
    $scope.medicio.higiene=false;
    $scope.medicio.bruts=false;
    $scope.medicio.brutsdos=false;
    $scope.medicio.mesclatemp=false;
    $scope.medicio.contaminacio=false;
    $scope.medicio.tempcaixa=-24;
    $scope.medicio.tempcaixamin=-30;
    $scope.medicio.tempcaixamax=-18;
    $scope.medicio.carnfresca=2;
    $scope.medicio.carnfrescamin=0;
    $scope.medicio.carnfrescamax=4;
    $scope.medicio.peixos=2;
    $scope.medicio.peixosmin=-1;
    $scope.medicio.peixosmax=4;
    $scope.medicio.envasats=true;
    $scope.medicio.organoleptiques=false;
    $scope.medicio.senseoval=false;
    $scope.medicio.marxants=false;
    $scope.medicio.senseetiqueta=false;
    $scope.medicio.defectuosos=false;

  }else if(laid==8){

    $scope.title="REFREDAMENT DE PLATS PREPARATS";
    $scope.medicio = {};
    $scope.medicio.superior=false;

  }else if(laid==9){

    $scope.title="CONTROL ENVASAMENT AL BUIT";
    $scope.medicio = {};
    $scope.medicio.buitperdut=false;
    $scope.medicio.malrealitzat=false;
    $scope.medicio.dataenvasat=true;
    $scope.medicio.refocong=1;

  }else if(laid==10){

    $scope.title="CONTROL DE NETEJA";

    $scope.medicio = {};
    $scope.medicio.zona=0;
    $scope.medicio.estat=0;



  }

  $scope.cambiazona = function(){

    $scope.estat=0;
  }

  $scope.cambiasubzona=function(elque){


    tmpstat=0;

    for(a=0; a<$scope.zones.length; a++){

      if($scope.medicio.zona==$scope.zones[a]['id']){


        for(b=0; b<$scope.zones[a]['subzona'].length; b++){  

          idsubzona=$scope.zones[a]['subzona'][b]['id'];

          if($scope.medicio.subzone[idsubzona]==false){
            tmpstat++;
          }
          
        }
      
      }

    }

    console.log(tmpstat);
    $scope.medicio.estat=tmpstat;

  }


  $scope.envia_medicio = function(recargapagina) {

      

      var id_section = $scope.laid;

      var now = new Date();
      // date en formato DDMMYYYY para key del array
      var date = now.getDate()+''+('0'+(now.getMonth()+1)).slice(-2)+''+now.getFullYear();

      // momento con time de la medicion
      var date_time = now.getDate()+'/'+('0'+(now.getMonth()+1)).slice(-2)+'/'+now.getFullYear()+' '+now.getHours()+':'+('0'+(now.getMinutes())).slice(-2)+':'+('0'+(now.getSeconds())).slice(-2);


      if(typeof window.localStorage['medicions'] == 'undefined'){
        buit = {};
        window.localStorage['medicions']=JSON.stringify(buit);
      }

      medicions=JSON.parse(window.localStorage['medicions']);

      console.log(medicions);

      if(typeof medicions[date.toString()] == 'undefined'){
        medicions[date.toString()]=[];
      }

      aguardar={
          'id' : id_section,
          'datetime' : date_time,
          'medicio' : $scope.medicio,
          'alert' : $scope.alertas
      }
      medicions[date.toString()].push(aguardar);

      window.localStorage['medicions']=JSON.stringify(medicions);


      var user = window.localStorage['app.User'];
      user = JSON.parse(user);

      $ionicLoading.show({template: '<center>Enviant dades...<br/> <ion-spinner class="spinner-light" icon="spiral"></ion-spinner></center>'});
      // envio $_POST al servicio para guardar mediciones
      // http://127.0.0.1/layflyadema/laiflyadema_api/medicion/save
      // http://nuk.isdeveloping.com/laiflyadema/backoffice/medicion/save

      /*console.log(JSON.stringify(medicions));
      console.log(user.id);
      console.log(id_section);
      console.log(user.local_id);*/

      $http.post(
        'http://nuk.isdeveloping.com/laiflyadema/backoffice/medicion/save',
        {
            medicion: medicions,
            user_id: user.id,
            module_id: id_section,
            local_id:  user.local_id
        }).
        then(function(response) {
            $ionicLoading.hide();
            if ( response.status == 200 ) {


                window.localStorage.removeItem('medicions');
                if(recargapagina!=false){
                  $state.go("app.novamedicio");
                }else{
                  $ionicLoading.show({template: '<center>Dades enviades<br/> <i class="ion-android-done ion-icon icon" style="font-size:36px;" icon="ion-android-done"></i></center>'});

                  $timeout( function(){

                    $ionicLoading.hide();
                    $state.go($state.current, {}, {reload: true});

                  },2000);

                }
            }else{
              alert("La medició s'ha guardat al teléfon, si us plau, feu \"sincronitzar\" a la secció corresponent un cop tingueu conexió.");
              $state.go("app.sincronitzar");
            }
        },
        function(response) { // error
          $ionicLoading.hide();
          alert("La medició s'ha guardat al teléfon, si us plau, feu \"sincronitzar\" a la secció corresponent un cop tingueu conexió.");
          $state.go("app.sincronitzar");

        }
      );
  }


  // Scope showing-question controller

  $scope.question=0;
  $scope.hayerrores=false;

  $scope.errorTemp = function(valor,maxim,minim,index,nom){
    console.log("Entro");
    if(valor>maxim || valor<minim){
      $scope.alertas[3]['equip'+index] = { 
        error : true, 
        title: 'Error de temperatura en l\'equip: '+nom+'. Temperatura actual: '+valor+'ºC. El valor correcte va entre: '+maxim+'ºC i '+minim+'ºC.', 
        msg : 'Aviseu a manteniment',
        module_id : 3 
      };

      console.log('Error de temperatura en l\'equip: '+nom+'. Temperatura actual: '+valor+'ºC. El valor correcte va entre: '+maxim+'ºC i '+minim+'ºC.');
    }else{
      
      $scope.alertas[3]['equip'+index] = { error : false, title: null, msg : null,module_id : 3 };

      console.log("OK!!!!");
    }

  }

  $scope.errorClor = function(valor,maxim,minim,modul){
    if(valor>maxim || valor<minim){
      if(modul=="1"){
        maxim=maxim/100;
        minim=minim/100;
        valor=valor/100;
      }

      $scope.alertas[modul]['clor'] = { 
        error : true, 
        title: 'Error de medició de clor. Clor actual: '+valor+'. El valor correcte va entre: '+maxim+' i '+minim+'.', 
        msg : 'Aviseu a manteniment',
        module_id : modul 
      };

      console.log('Error de medició de clor. Clor actual: '+valor+'. El valor correcte va entre: '+maxim+' i '+minim+'.');
    }else{
      
      $scope.alertas[modul]['clor'] = { error : false, title: null, msg : null,module_id : modul };

      console.log("OK!!!!");
    }

  }

  $scope.setLastAnswer=function(num){

    $timeout( function(){ 
      
      $scope.hayerrores=false;

      angular.forEach($scope.alertas[7], function(value, key) {
          if(value['error']==true){ 
            $scope.hayerrores=true; 
            $scope.question=value['question']; 
          }
      });

      if($scope.hayerrores==true){
        console.log('!!ERROR!! || num: '+num+' || question:'+$scope.question);
        console.log($scope.alertas);
      }else{
        

        $scope.hayerrores=false;
        

        if(num==4){
          if($scope.medicio.tecarn==false || $scope.medicio.tepeix==false){
            num++;
          }
        }

        if(num==5){
          if($scope.medicio.tefruita==false){
            num++;
          }
        }


        if(num==8){
          if($scope.medicio.tecongelats==false){
            num++;
            console.log("ONE MORE!");
          }
        }

        if(num==9){
          if($scope.medicio.tecarn==false){
            num++;
            console.log("ONE MORE!");
          }
        }

        if(num==10){
          if($scope.medicio.tepeix==false){
            num++;
          }
        }

        if(num==14){
          if($scope.medicio.tecarn==false){
            num++;
          }
        }

        $scope.question=num+1;

        console.log('NO ERROR! ESTEM A LA:'+$scope.question);

        $ionicScrollDelegate.resize();
      }

    }, 50);


  }


})

/*  NovamedicioCtrl. Afegir una medició */
.controller('SinzronitzaCtrl', function($scope, $state, $http, $ionicLoading) {


  $scope.sincro = function(){

    var user = window.localStorage['app.User'];
    var medicions = JSON.parse(window.localStorage['medicions']);

    user = JSON.parse(user);

    $ionicLoading.show({template: '<center>Enviant dades...<br/> <ion-spinner class="spinner-light" icon="spiral"></ion-spinner></center>'});

    $http.post(
      'http://nuk.isdeveloping.com/laiflyadema/backoffice/medicion/save',
      {
          medicion: medicions,
          user_id: user.id,
          local_id:  user.local_id
      }).
      then(function(response) {
          $ionicLoading.hide();
          if ( response.status == 200 ) {
              window.localStorage.removeItem('medicions');
          }else{
            alert("Hi ha hagut un error al sincronitzar. Si us plau, intenteu-ho una altre vegada.");
          }
      },
      function(response) { // error
        $ionicLoading.hide();
      }
    );

  }
  //edicions=window.localStorage('medicions');
  //window.localStorage.removeItem('medicions');

})


/**
 * IncidenciasCtrl
 * Listado de Mediciones ordenadas por fecha en las que existen Alertas
 */
.controller('IncidenciasCtrl', function($scope, $state, $http, $ionicLoading) {

    var listadoIncidencias = window.localStorage['app.Incidencias'];

    if ( angular.isDefined(listadoIncidencias) && listadoIncidencias.length > 0 ) {
        $scope.listadoIncidencias = angular.fromJson(listadoIncidencias);
        console.log($scope.listadoIncidencias);
    }

    $scope.toggleGroup = function(group) {
        if ($scope.isGroupShown(group)) {
            $scope.shownGroup = null;
        } else {
            $scope.shownGroup = group;
        }
    };

    $scope.isGroupShown = function(group) {
        return $scope.shownGroup === group;
    };

    // $scope.get = function(){
    $ionicLoading.show({template: '<center>Conectant...<br/> <ion-spinner class="spinner-light" icon="spiral"></ion-spinner></center>'});

    // Obtenemos las ultimas Mediciones
    $http.get('http://nuk.isdeveloping.com/laiflyadema/backoffice/api/medicion/alertas/'+$scope.user.local_id+'.json').
        then(function(response) {

          console.log(response);

            $ionicLoading.hide();

            if ( response.status == 200 ) {

              if(window.localStorage['app.Incidencias']!=JSON.stringify(response.data.mediciones)){
                window.localStorage['app.Incidencias'] = JSON.stringify(response.data.mediciones);
                $state.go($state.current, {}, {reload: true});
              }

            }
        },
        function(response) { 

          console.log(response);
            $ionicLoading.hide();
        }
    );




})
/*  end IncidenciasCtrl. */


/*  NovamedicioCtrl. Afegir una medició */
.controller('MedicionesCtrl', function($scope, $state, $http, $ionicLoading) {


    $scope.toggleGroup = function(group) {
        if ($scope.isGroupShown(group)) {
            $scope.shownGroup = null;
        } else {
            $scope.shownGroup = group;
        }
    };

    $scope.isGroupShown = function(group) {
        return $scope.shownGroup === group;
    };

    var listadoMediciones = window.localStorage['app.MedicionesListado'];


    if ( angular.isDefined(listadoMediciones) && listadoMediciones.length > 0 ) {
        $scope.listadoMediciones = angular.fromJson(listadoMediciones);
    }

    medicionesObj=JSON.parse(listadoMediciones);

    $scope.agrupadas={};

    angular.forEach(medicionesObj, function(value, key) {
      tmp=value['date'].split(" ");
      medicionesObj[key]['date']=tmp[0];
    });

    angular.forEach(medicionesObj, function(value, key) {
      tmp=value['date'].split(" ");
      $scope.agrupadas[tmp[0]]=[];
    });

    angular.forEach(medicionesObj, function(value, key) {
      tmp=value['date'].split(" ");
      $scope.agrupadas[tmp[0]].push(value);
    });

    $scope.agrupadasArray=[];


    angular.forEach($scope.agrupadas, function(value, key) {
      $scope.agrupadasArray[$scope.agrupadasArray.length]=value;
    });


    $ionicLoading.show({template: '<center>Conectant...<br/> <ion-spinner class="spinner-light" icon="spiral"></ion-spinner></center>'});

    // Obtenemos las ultimas Mediciones
    $http.get('http://nuk.isdeveloping.com/laiflyadema/backoffice/api/medicion/all/'+$scope.user.local_id+'.json').
        then(function(response) {

             if ( response.status == 200 ) {
              if(window.localStorage['app.MedicionesListado']!=JSON.stringify(response.data.mediciones)){
                window.localStorage['app.MedicionesListado'] = JSON.stringify(response.data.mediciones);
                $state.go($state.current, {}, {reload: true});
              }else{

              }
            }

            $ionicLoading.hide();
        },
        function(response) {

          $ionicLoading.hide();
        }
    );

});
/*  end NovamedicioCtrl. Afegir una medició*/
