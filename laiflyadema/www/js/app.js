// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $httpProvider, $ionicConfigProvider) {

  $httpProvider.defaults.useXDomain = true;
  delete $httpProvider.defaults.headers.common['X-Requested-With'];

  // $ionicConfigProvider.views.maxCache(0);

  $stateProvider.state('inicio', {
    url: "/inicio",
    abstract: false,
    templateUrl: "templates/inicio.html",
    controller: 'InicioCtrl'
  })

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })

  .state('app.benvinguts', {
    url: "/benvinguts",
    views: {
      'menuContent': {
        templateUrl: "templates/benvinguts.html"
      }
    }
  })

  .state('app.novamedicio', {
    url: "/novamedicio",
    cache : false,
    views: {
      'menuContent': {
        templateUrl: "templates/novamedicio.html",
        controller : 'NovamedicioCtrl'
      }
    }
  })

  .state('app.medicio', {
    url: "/medicio/:laid",
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "templates/medicio.html"
      }
    }
  })

  .state('app.incidencies', {
    url: "/incidencies",
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "templates/incidencies.html",
        controller : 'IncidenciasCtrl'
      }
    }
  })

  .state('app.medicions', {
    url: "/medicions",
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "templates/medicions.html",
        controller : 'MedicionesCtrl'
      }
    }
  })

  .state('app.logout', {
    url: '/logout',
    abstract: true,
    views: {
      'menuContent': {
        templateUrl: "templates/inicio.html"
      }
    }
  })

  .state('app.sincronitzar', {
    url: "/sincronitzar",
    views: {
      'menuContent': {
        templateUrl: "templates/sincronitzar.html"
      }
    }
  });


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/inicio');

});
