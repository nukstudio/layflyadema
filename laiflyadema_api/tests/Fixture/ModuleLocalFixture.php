<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ModuleLocalFixture
 *
 */
class ModuleLocalFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'module_local';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'module_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'local_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'FK_modulelocal_module_idx' => ['type' => 'index', 'columns' => ['module_id'], 'length' => []],
            'FK_modulelocal_local_idx' => ['type' => 'index', 'columns' => ['local_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'FK_modulelocal_module' => ['type' => 'foreign', 'columns' => ['module_id'], 'references' => ['module', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'FK_modulelocal_local' => ['type' => 'foreign', 'columns' => ['local_id'], 'references' => ['local', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'module_id' => 1,
            'local_id' => 1,
            'created' => '2015-09-15 16:28:43',
            'modified' => '2015-09-15 16:28:43'
        ],
    ];
}
