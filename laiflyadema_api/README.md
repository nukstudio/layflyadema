## API services

Todas las llamadas al servicio se encuentran bajo `/api/{controller}.json`

`Cliente` corresponde con la persona/empresa física
```
getClients => /api/client.json
getClientsById => /api/client/{id}.json

{
"client": [
    {
        "id": 1,
        "name": "Cliente de prueba 1",
        "cif": "B22333444",
        "email": "email@cliente1.com",
        "phone": "665910227",
        "enabled": true,
        "created": null,
        "modified": null
    },
}
```

##

`Local` corresponde con los establecimientos que pertenecen al `Cliente`
```
getLocales => /api/local.json
getLocalesById => /api/local/{id}.json
getLocalesByUserId => /api/local/user/{id}.json   (TO-DO con estructura Cliente)
```

##

`User` corresponde con el acceso suministrado a un trabajador para el uso de la app en un determinado `Local`
```
getUsers => /api/user.json
getUsersById => /api/user/{id}.json

{
    "user": {
        "id": 1,
        "client_id": 1,
        "local_id": 1,
        "username": "sergio@nukstudio.com",
        "password": "$2y$10$5Dek2B9eGS.CWrM6ls4zOu.WSvjRPV.v7RqZaifbICKepXP3wlG3m",
        "auth_key": null,
        "created": "2015-09-15T14:37:57+0000",
        "modified": "2015-09-16T10:37:34+0000",
        "local": {
            "id": 1,
            "client_id": 1,
            "name": "Local de prueba 1",
            "address": "Carrer d\u0027Aribau, 205. Barcelona",
            "created": null,
            "modified": null
        },
        "client": {
            "id": 1,
            "name": "Cliente de prueba 1",
            "cif": "B22333444",
            "email": "email@cliente1.com",
            "phone": "665910227",
            "enabled": true,
            "created": null,
            "modified": null
        }
    }
}
```

##

`Module` es un listado de todas las secciones disponibles para hacer mediciones.
(únicamente es un listado de los modulos que pueden ser gestionados en la app)
```
getModules => /api/module.json
getModulesById => /api/module/{id}.json

{
    "module": {
        "id": 1,
        "name": "CONTROL RECEPCI\u00d3 MERCADERIA",
        "created": null,
        "modified": null
    }
}
```

##

`ModuleLocal` corresponde con la asociación del `Module` al `Local` para habilitar determinadas mediciones a un cliente/usuario
```
getModuleLocal => /api/module_local.json
getModuleLocalByLocalId => /api/module_local/{id_local}.json

{
    "module_local": [
        {
            "id": 1,
            "module_id": 1,
            "local_id": 1,
            "created": null,
            "modified": null,
            "module": {
                "id": 1,
                "name": "CONTROL RECEPCI\u00d3 MERCADERIA",
                "created": null,
                "modified": null
            }
        },
        {
            "id": 2,
            "module_id": 2,
            "local_id": 1,
            "created": null,
            "modified": null,
            "module": {
                "id": 2,
                "name": "CONTROL TEMPERATURES DELS EQUIPS DE FRED",
                "created": null,
                "modified": null
            }
        },
    ]
}
```

##

`Provider` corresponde con un listado de Proveedores asociados a un `Local`
```
getProviders => /api/provider.json
getProviderById => /api/provider/{id_provider}.json
getProviderByLocalId => /api/provider/local/{id_local}.json
{
    "provider": [
        {
            "id": 2,
            "client_id": 1,
            "local_id": 1,
            "name": "Proveedor 2",
            "description": "Descripci\u00f3n para proveedor 2",
            "congelados": false,
            "carne": false,
            "pescado": false,
            "fruta": false,
            "created": null,
            "modified": null
        },
        {
            "id": 3,
            "client_id": 1,
            "local_id": 1,
            "name": "Proveedor 3",
            "description": "Descripci\u00f3n para proveedor 3",
            "congelados": false,
            "carne": false,
            "pescado": false,
            "fruta": false,
            "created": null,
            "modified": null
        },
    ]
}
```

##

`Equipo` corresponde con un listado de los equipos configurados en un `Local`
```
getEquipos => /api/equipo.json
getEquiposById => /api/equipo/{id_equipo}.json
getEquiposByLocalId => /api/equipo/local/{id_local}.json
{
    "equipo": [
        {
            "id": 1,
            "local_id": 1,
            "name": "Cambra peix",
            "max": 3,
            "min": 0,
            "standard": 1,
            "created": null,
            "modified": null
        },
    ]
}
```

##

`Zona` corresponde con un listado de las Zonas de limpieza configurados en un `Local`
```
getZonas => /api/zona.json
getZonaById => /api/zona/{id_equipo}.json
getZonaByLocalId => /api/zona/local/{id_local}.json
{
    "zona": [
        {
            "id": 1,
            "local_id": 1,
            "name": "Zona 1",
            "created": "2015-09-23T14:39:37+0000",
            "modified": "2015-09-23T14:39:37+0000",
            "local": {
                "id": 1,
                "client_id": 1,
                "name": "Local de prueba 1",
                "address": "Carrer d\u0027Aribau, 205. Barcelona",
                "created": null,
                "modified": null
            }
        }
    ]
}
```
##


`Subzona` corresponde con un listado de las subzonas de limpieza
```
getSubZonas => /api/subzona/zona/{id_zona}.json

{
    "subzona": [
        {
            "id": 1,
            "zona_id": 1,
            "name": "test",
            "created": null,
            "modified": null,
            "zona": {
                "id": 1,
                "local_id": 2,
                "name": "Zona 1",
                "created": "2015-09-23T14:39:37+0000",
                "modified": "2015-09-23T14:39:37+0000"
            }
        }
    ]
}
```
##
`Mediciones`
```
getAllMedicionesByLocaId => /api/medicion/all/{local_id}.json

{
    "mediciones": [
        {
            "id": 268,
            "user_id": 1,
            "local_id": 1,
            "date": "29\/09\/2015 00:00",
            "hour": "29\/09\/2015 15:56",
            "module_id": 3,
            "data": "",
            "alert": "",
            "created": null,
            "modified": null,
            "module": {
                "id": 3,
                "name": "CONTROL DE NETEJA",
                "created": null,
                "modified": null
            },
            "local": {
                "id": 1,
                "client_id": 1,
                "name": "Local de prueba 1",
                "address": "Carrer d\u0027Aribau, 205. Barcelona",
                "created": null,
                "modified": null
            },
            "isAlert": false,
            "countAlerts": 0
        },
```
