<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Local Controller
 *
 * @property \App\Model\Table\LocalTable $Local
 */
class LocalController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->Auth->allow();
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

    }


    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $session = $this->request->session()->read();

        $this->paginate = [
            'contain' => [
                'Client',
                'Users',
                'ModuleLocal',
            ]
        ];

        if ( isset($session['Auth']['User']['role']) && $session['Auth']['User']['role'] == 'user' ) {
            $this->set('local', $this->paginate($this->Local->find('all',
                                         ['conditions' => ['Local.id' => $session['Auth']['User']['local_id']],])));

            $this->set('user_role', $session['Auth']['User']['role']);
        }
        else {
            $this->set('local', $this->paginate($this->Local));
        }

        $this->set('_serialize', ['local']);
    }

    /**
     * View method
     *
     * @param string|null $id Local id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $local = $this->Local->get($id, [
            'contain' => [
                'Client',
                'Users',
                'ModuleLocal',
            ]
        ]);
        $this->set('local', $local);
        $this->set('_serialize', ['local']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $local = $this->Local->newEntity();
        if ($this->request->is('post')) {
            $local = $this->Local->patchEntity($local, $this->request->data);
            if ($this->Local->save($local)) {
                $this->Flash->success(__('The local has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The local could not be saved. Please, try again.'));
            }
        }
        $client = $this->Local->Client->find('list', ['limit' => 200]);
        $this->set(compact('local', 'client'));
        $this->set('_serialize', ['local']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Local id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $local = $this->Local->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $local = $this->Local->patchEntity($local, $this->request->data);
            if ($this->Local->save($local)) {
                $this->Flash->success(__('The local has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The local could not be saved. Please, try again.'));
            }
        }


        $client = $this->Local->Client->find('list', ['limit' => 200]);
        $this->set(compact('local', 'client'));
        $this->set('_serialize', ['local']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Local id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $local = $this->Local->get($id);
        if ($this->Local->delete($local)) {
            $this->Flash->success(__('The local has been deleted.'));
        } else {
            $this->Flash->error(__('The local could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }


    // api requests
    public function getByUserId() {
        $local = $this->Local->find('all')
            ->contain(['Users']);

        $this->set('local', $local);
        $this->set('_serialize', ['local']);
    }


}
