<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * ModuleLocal Controller
 *
 * @property \App\Model\Table\ModuleLocalTable $ModuleLocal
 */
class ModuleLocalController extends AppController
{

    public function initialize()
    {
        parent::initialize();

        // added for json returns
        $this->loadComponent('RequestHandler');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Auth->allow();
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Module', 'Local']
        ];
        $this->set('moduleLocal', $this->paginate($this->ModuleLocal));
        $this->set('_serialize', ['moduleLocal']);
    }

    /**
     * View method
     *
     * @param string|null $id Module Local id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $moduleLocal = $this->ModuleLocal->get($id, [
            'contain' => ['Module', 'Local']
        ]);
        $this->set('moduleLocal', $moduleLocal);
        $this->set('_serialize', ['moduleLocal']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $moduleLocal = $this->ModuleLocal->newEntity();
        if ($this->request->is('post')) {
            $moduleLocal = $this->ModuleLocal->patchEntity($moduleLocal, $this->request->data);
            if ($this->ModuleLocal->save($moduleLocal)) {
                $this->Flash->success(__('The module local has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The module local could not be saved. Please, try again.'));
            }
        }
        $module = $this->ModuleLocal->Module->find('list', ['limit' => 200]);
        $local = $this->ModuleLocal->Local->find('list', ['limit' => 200]);
        $this->set(compact('moduleLocal', 'module', 'local'));
        $this->set('_serialize', ['moduleLocal']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Module Local id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $moduleLocal = $this->ModuleLocal->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $moduleLocal = $this->ModuleLocal->patchEntity($moduleLocal, $this->request->data);
            if ($this->ModuleLocal->save($moduleLocal)) {
                $this->Flash->success(__('The module local has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The module local could not be saved. Please, try again.'));
            }
        }
        $module = $this->ModuleLocal->Module->find('list', ['limit' => 200]);
        $local = $this->ModuleLocal->Local->find('list', ['limit' => 200]);
        $this->set(compact('moduleLocal', 'module', 'local'));
        $this->set('_serialize', ['moduleLocal']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Module Local id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $moduleLocal = $this->ModuleLocal->get($id);
        if ($this->ModuleLocal->delete($moduleLocal)) {
            $this->Flash->success(__('The module local has been deleted.'));
        } else {
            $this->Flash->error(__('The module local could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function getModuleLocalByLocalId($id = null)
    {

        $query = $this->ModuleLocal
            ->find()
            ->where(['ModuleLocal.local_id' => $id])
            ->contain(['Module']);

        $results = $query->all();

        $this->set('module_local', $results);
        $this->set('_serialize', ['module_local']);
    }
}
