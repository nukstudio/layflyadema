<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Equipo Controller
 *
 * @property \App\Model\Table\EquipoTable $Equipo
 */
class EquipoController extends AppController
{

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');

        // allow all (testing)
        $this->Auth->allow();
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $session = $this->request->session()->read();

        $this->paginate = [
            'contain' => ['Local']
        ];


        if ( isset($session['Auth']['User']['role']) && $session['Auth']['User']['role'] == 'user' ) {
            $this->set('equipo', $this->paginate($this->Equipo->find('all',
                                         ['conditions' => ['Equipo.local_id' => $session['Auth']['User']['local_id']],])));
        }
        else {
            $this->set('equipo', $this->paginate($this->Equipo));
        }

        $this->set('_serialize', ['equipo']);
    }

    /**
     * View method
     *
     * @param string|null $id Equipo id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $equipo = $this->Equipo->get($id, [
            'contain' => ['Local']
        ]);
        $this->set('equipo', $equipo);
        $this->set('_serialize', ['equipo']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $equipo = $this->Equipo->newEntity();
        if ($this->request->is('post')) {
            $equipo = $this->Equipo->patchEntity($equipo, $this->request->data);
            if ($this->Equipo->save($equipo)) {
                $this->Flash->success(__('The equipo has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The equipo could not be saved. Please, try again.'));
            }
        }


        $session = $this->request->session()->read();

        if ( isset($session['Auth']['User']['role']) && $session['Auth']['User']['role'] == 'user' ) {
            $local = $this->Equipo->Local->find('list',
                ['conditions' => ['Local.id' => $session['Auth']['User']['local_id']],
                 'limit' => 1]);
        }
        else {
            $local = $this->Equipo->Local->find('list', ['limit' => 200]);
        }

        $this->set(compact('equipo', 'local'));
        $this->set('_serialize', ['equipo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Equipo id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $session = $this->request->session()->read();

        $equipo = $this->Equipo->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $equipo = $this->Equipo->patchEntity($equipo, $this->request->data);
            if ($this->Equipo->save($equipo)) {
                $this->Flash->success(__('The equipo has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The equipo could not be saved. Please, try again.'));
            }
        }

        if ( isset($session['Auth']['User']['role']) && $session['Auth']['User']['role'] == 'user' ) {
            $local = $this->Equipo->Local->find('list',
                ['conditions' => ['Local.id' => $session['Auth']['User']['local_id']],
                 'limit' => 1]);
        }
        else {
            $local = $this->Equipo->Local->find('list', ['limit' => 200]);
        }

        $this->set(compact('equipo', 'local'));
        $this->set('_serialize', ['equipo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Equipo id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $equipo = $this->Equipo->get($id);
        if ($this->Equipo->delete($equipo)) {
            $this->Flash->success(__('The equipo has been deleted.'));
        } else {
            $this->Flash->error(__('The equipo could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function getEquipoByLocalId($id = null)
    {

        $query = $this->Equipo
            ->find()
            ->where(['Equipo.local_id' => $id]);

        $results = $query->all();

        $this->set('equipo', $results);
        $this->set('_serialize', ['equipo']);
    }
}
