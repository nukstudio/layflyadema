<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Subzona Controller
 *
 * @property \App\Model\Table\SubzonaTable $Subzona
 */
class SubzonaController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');

        // allow all (testing)
        $this->Auth->allow();
    }


    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Zona']
        ];
        $this->set('subzona', $this->paginate($this->Subzona));
        $this->set('_serialize', ['subzona']);
    }

    /**
     * View method
     *
     * @param string|null $id Subzona id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $subzona = $this->Subzona->get($id, [
            'contain' => ['Zona']
        ]);
        $this->set('subzona', $subzona);
        $this->set('_serialize', ['subzona']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $subzona = $this->Subzona->newEntity();
        $session = $this->request->session()->read('Auth.User');

        if ($this->request->is('post')) {
            $subzona = $this->Subzona->patchEntity($subzona, $this->request->data);
            if ($this->Subzona->save($subzona)) {
                $this->Flash->success(__('The subzona has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The subzona could not be saved. Please, try again.'));
            }
        }

        if ( isset($session['role']) && $session['role'] == 'user' ) {
            $zonas = $this->Subzona->Zona->find('list', ['limit' => 200 , 'conditions' => ['id'=>$session['local_id']]]);
        }else if ( isset($session['role']) && $session['role'] == 'admin' ) {
            $zonas = $this->Subzona->Zona->find('list', ['limit' => 200]);
        }

        $this->set(compact('subzona', 'zonas'));
        $this->set('_serialize', ['subzona']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Subzona id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $subzona = $this->Subzona->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $subzona = $this->Subzona->patchEntity($subzona, $this->request->data);
            if ($this->Subzona->save($subzona)) {
                $this->Flash->success(__('The subzona has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The subzona could not be saved. Please, try again.'));
            }
        }
        $zonas = $this->Subzona->Zona->find('list', ['limit' => 200]);
        $this->set(compact('subzona', 'zonas'));
        $this->set('_serialize', ['subzona']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Subzona id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $subzona = $this->Subzona->get($id);
        if ($this->Subzona->delete($subzona)) {
            $this->Flash->success(__('The subzona has been deleted.'));
        } else {
            $this->Flash->error(__('The subzona could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }


    public function getSubZonaByZonaId($id = null)
    {

        $query = $this->Subzona
            ->find()
            ->contain(['Zona'])
            ->where(['Zona.id' => $id]);

        $results = $query->all();

        $this->set('subzona', $results);
        $this->set('_serialize', ['subzona']);
    }
}
