<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Zona Controller
 *
 * @property \App\Model\Table\ZonaTable $Zona
 */
class ZonaController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');

        // allow all (testing)
        $this->Auth->allow();
    }



    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $session = $this->request->session()->read();

        $this->paginate = [
            'contain' => ['Local', 'Subzona']
        ];


        if ( isset($session['Auth']['User']['role']) && $session['Auth']['User']['role'] == 'user' ) {
            $this->set('zona', $this->paginate($this->Zona->find('all',
                                         ['conditions' => ['Zona.local_id' => $session['Auth']['User']['local_id']],])));
        }
        else {
            $this->set('zona', $this->paginate($this->Zona));
        }

        $this->set('_serialize', ['zona']);
    }

    /**
     * View method
     *
     * @param string|null $id Zona id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $zona = $this->Zona->get($id, [
            'contain' => ['Local', 'Subzona']
        ]);

        $this->set('zona', $zona);
        $this->set('_serialize', ['zona']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $zona = $this->Zona->newEntity();
        $session = $this->request->session()->read('Auth.User');
        if ($this->request->is('post')) {
            $zona = $this->Zona->patchEntity($zona, $this->request->data);
            if ($this->Zona->save($zona)) {
                $this->Flash->success(__('The zona has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The zona could not be saved. Please, try again.'));
            }
        }

        if ( isset($session['role']) && $session['role'] == 'user' ) {
            $local = $this->Zona->Local->find('list', ['limit' => 200, 'conditions' => ['id'=>$session['local_id']]]);
        }else if ( isset($session['role']) && $session['role'] == 'admin' ) {
            $local = $this->Zona->Local->find('list', ['limit' => 200]);
        }
        $this->set(compact('zona', 'local'));
        $this->set('_serialize', ['zona']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Zona id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $zona = $this->Zona->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $zona = $this->Zona->patchEntity($zona, $this->request->data);
            if ($this->Zona->save($zona)) {
                $this->Flash->success(__('The zona has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The zona could not be saved. Please, try again.'));
            }
        }

         $session = $this->request->session()->read();

        if ( isset($session['Auth']['User']['role']) && $session['Auth']['User']['role'] == 'user' ) {
            $local = $this->Zona->Local->find('list',
                ['conditions' => ['Local.id' => $session['Auth']['User']['local_id']],
                 'limit' => 1]);
        }
        else {
            $local = $this->Zona->Local->find('list', ['limit' => 200]);
        }

        $this->set(compact('zona', 'local'));
        $this->set('_serialize', ['zona']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Zona id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $zona = $this->Zona->get($id);
        if ($this->Zona->delete($zona)) {
            $this->Flash->success(__('The zona has been deleted.'));
        } else {
            $this->Flash->error(__('The zona could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function getZonaByLocalId($id = null)
    {

        $query = $this->Zona->find('all')
            ->where(['Zona.local_id' => $id])
                ->contain(['Subzona']);

        $results = $query->all();

        $this->set('zona', $results);
        $this->set('_serialize', ['zona']);
    }
}
