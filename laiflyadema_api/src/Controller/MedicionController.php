<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

use Cake\I18n\Time;
use Cake\Network\Email\Email;
use App\Model\Entity\Medicion;
use App\Model\Entity\Local;
use App\Model\Entity\Cliente;
use App\Model\Entity\Module;
use App\Model\Entity\Zona;
use App\Model\Entity\Equipo;

/**
 * Medicion Controller
 *
 * @property \App\Model\Table\MedicionTable $Medicion
 */
class MedicionController extends AppController
{

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->Auth->allow();
    }


    private function getAlerts($id, $limit = 20) {
        Time::setJsonEncodeFormat('./MM/yyy HH:mm');

        $medicion = new Medicion();

        if ( $id == 'all') {
            // Obtenemos todas las mediciones
            $query = $this->Medicion
                ->find('all')
                ->contain(['Local', 'Module'])
                ->limit($limit)
                ->order('Medicion.hour DESC');
        }
        else {
            // Obtenemos todas las mediciones
            $query = $this->Medicion
                ->find('all')
                ->contain(['Local', 'Module'])
                ->where(['Medicion.local_id' => $id])
                ->limit($limit)
                ->order('Medicion.hour DESC');
        }

        $results = $query->all();

        $aux = array();

        foreach ($results as $key => $value) {
            $date = explode('/', substr($value['date'], 0, 8));
            $_key = trim($date[0].$date[1].$date[2]);

            $arr_alerta = $medicion->sanitizeAlerts($value->alert);
            $isAlert = $medicion->isAlert($arr_alerta);

            if ( isset($isAlert) && $isAlert ) {
                // $aux[$_key]['day']   = $date[0];
                // $aux[$_key]['month'] = $this->trad_months[$date[1]];
                // $aux[$_key]['year']  = trim($date[2]);

                $aux[$_key][$key]['incidencias'] = $value;
                $aux[$_key][$key]['incidencias']['alert'] = $arr_alerta;
                $aux[$_key][$key]['isAlert'] = $medicion->isAlert($arr_alerta);
                $aux[$_key][$key]['countAlerts'] = $medicion->countAlert($arr_alerta);
            }
        }

        return $aux;
    }


    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $date_from = $this->request->data('date_from');
        $date_to   = $this->request->data('date_to');
        $medicion_type = $this->request->data('medicion_type');

        $results = $this->getMedicionForIndex($date_from, $date_to, $medicion_type);

        $medicion = $results['medicion'];
        $zonas    = $results['zonas'];
        $equipos  = $results['equipos'];


        $_download = $this->request->query('download');

        if ( isset($_download) && $_download == 'pdf') {
            $download_pdf = $this->download_pdf($medicion);

            $this->set('download_pdf', $download_pdf);
        }


        $this->set('aux', $medicion);
        $this->set('equipos', $equipos);
        $this->set('zonas', $zonas);
        $this->set('_serialize', ['aux']);
    }

    /**
     * View method
     *
     * @param string|null $id Medicion id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $medicion = $this->Medicion->get($id, [
            'contain' => ['Users', 'Locals', 'Modules']
        ]);
        $this->set('medicion', $medicion);
        $this->set('_serialize', ['medicion']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $medicion = $this->Medicion->newEntity();
        if ($this->request->is('post')) {
            $medicion = $this->Medicion->patchEntity($medicion, $this->request->data);
            if ($this->Medicion->save($medicion)) {
                $this->Flash->success(__('The medicion has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The medicion could not be saved. Please, try again.'));
            }
        }
        $users = $this->Medicion->Users->find('list', ['limit' => 200]);
        $locals = $this->Medicion->Locals->find('list', ['limit' => 200]);
        $modules = $this->Medicion->Modules->find('list', ['limit' => 200]);
        $this->set(compact('medicion', 'users', 'locals', 'modules'));
        $this->set('_serialize', ['medicion']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Medicion id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {

        $medicion = $this->Medicion->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $medicion = $this->Medicion->patchEntity($medicion, $this->request->data);
            if ($this->Medicion->save($medicion)) {
                $this->Flash->success(__('The medicion has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The medicion could not be saved. Please, try again.'));
            }
        }
        $users = $this->Medicion->Users->find('list', ['limit' => 200]);
        $locals = $this->Medicion->Locals->find('list', ['limit' => 200]);
        $modules = $this->Medicion->Modules->find('list', ['limit' => 200]);
        $this->set(compact('medicion', 'users', 'locals', 'modules'));
        $this->set('_serialize', ['medicion']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Medicion id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $medicion = $this->Medicion->get($id);
        if ($this->Medicion->delete($medicion)) {
            $this->Flash->success(__('The medicion has been deleted.'));
        } else {
            $this->Flash->error(__('The medicion could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }




    public function save() {
        $this->autoRender = false;

        // get $_POST
        $arr_mediciones = $this->request->data('medicion');
        $user_id        = $this->request->data('user_id');
        $local_id       = $this->request->data('local_id');
        $id_section     = $this->request->data('module_id');

        $medicion = TableRegistry::get('Medicion');

        if ( isset($arr_mediciones) && isset($user_id) && isset($local_id) ) {

            foreach ($arr_mediciones as $fecha => $data) {

                foreach ($data as $key => $value) {
                    $fecha = \DateTime::createFromFormat('d/m/Y H:i:s',  $value['datetime']);

                    $_data  = json_encode($value['medicio']);
                    $_alert = json_encode($value['alert']);

                    $aux = array(
                        'user_id' => $user_id,
                        'local_id' => $local_id,
                        'module_id' => $value['id'],
                        'date' => $fecha->format('Y-m-d'),
                        'hour' => $fecha->format('Y-m-d H:i:s'),
                        'data' =>  $_data,
                        'alert' => $_alert,
                    );

                    $query = $medicion->query()
                        ->insert(['user_id',
                                  'local_id',
                                  'module_id',
                                  'date',
                                  'hour',
                                  'data',
                                  'alert'])
                        ->values($aux)
                        ->execute();

                    try {
                        // envio de email
                        $this->sendEmail($_alert, $local_id, $value['id'], $fecha->format('Y-m-d H:i:s'));
                    } catch (Exception $e) {

                    }

                }
            }
        }
    }

    /* Obtenemos un listado de las mediciones para mostrar en el Backoffice */
    private function getMedicionForIndex($date_from, $date_to, $medicion_type = 0)
    {
        $session = $this->request->session()->read();
        $this->Zona   = TableRegistry::get('Zona');
        $this->Equipo = TableRegistry::get('Equipo');


        // condiciones
        $conditions=array();
        $aux_conditions=array();

        // condiciones de fechas
        if ( !empty($date_from) && !empty($date_to) ) {
            $from = \DateTime::createFromFormat('d/m/Y', $date_from);
            $to   = \DateTime::createFromFormat('d/m/Y', $date_to);

            $conditions['Medicion.hour >='] = $from->format('Y-m-d').' 00:00:00';
            $conditions['Medicion.hour <='] = $to->format('Y-m-d').' 23:59:59';
        }else{
            $conditions['Medicion.hour >'] = new \DateTime('-30 days');
        }

        // echo $from->format('Y-m-d 00:00:00').'<br >';;
        // echo $to->format('Y-m-d 00:00:00');;

        //condiciones de tipo de medicion
        if ( !empty($medicion_type) && $medicion_type!=0){
            $conditions['Medicion.module_id'] = $medicion_type;
        }


        // condiciones de tipo de usuario
        if ( isset($session['Auth']['User']['role']) && $session['Auth']['User']['role'] == 'user' ) {
            $conditions['Medicion.local_id'] = $aux_conditions['local_id'] = $session['Auth']['User']['local_id'];
        }


        // Queries o no queries?

        $medicion = $this->Medicion->find('all', [
            'conditions' => $conditions,
            'contain'    => ['Users','Local', 'Module'],
            'order'      => 'Medicion.hour DESC',
            'limit'      => 50,
        ]);

        $zonas = $this->Zona->find('all', [
            'conditions' => $aux_conditions,
            'contain'    => ['Subzona']
        ]);

        $equipos = $this->Equipo->find('all', [
            'conditions' => $aux_conditions
        ]);

        return array('medicion' => $medicion, 'zonas' => $zonas, 'equipos' => $equipos);

    }

    private function download_pdf($medicion = array()) {
        $folder_url = null;

        if ( !empty($medicion) )
        {
            // Generamos el PDF
            $CakePdf = new \CakePdf\Pdf\CakePdf();
            $CakePdf->template('medicion', 'default');
            $CakePdf->viewVars(array('medicion' => $medicion));

            //get the pdf string returned
            //$pdf = $CakePdf->output();

            //or write it to file directly
            $file_name = 'medicio_'.date('YmdHis').'.pdf';
            $folder_webroot = WWW_ROOT.'files'.DS.'backoffice'.DS.$file_name;
            $folder_url = 'files'.DS.'backoffice'.DS.$file_name;

            $pdf = $CakePdf->write($folder_webroot);
        }

        return $folder_url;
    }



    public function getAll($id = null)
    {
        Time::setJsonEncodeFormat('dd/MM/yyy HH:mm');

        $query = $this->Medicion
            ->find('all')
            ->contain(['Local', 'Module'])
            ->where(['Medicion.local_id' => $id])
            ->limit(20)
            ->order('Medicion.date DESC');

        $results = $query->all();

        $this->set('mediciones', $results);
        $this->set('_serialize', ['mediciones']);
    }

    public function getAllAlertas($id = null, $limit = 20)
    {
        Time::setJsonEncodeFormat('dd/MM/yyy HH:mm');

        $medicion = new Medicion();

        // Obtenemos todas las mediciones
        $query = $this->Medicion
            ->find('all')
            ->contain(['Local', 'Module'])
            ->where(['Medicion.local_id' => $id])
            ->limit($limit)
            ->order('Medicion.hour DESC');

        $results = $query->all();

        $aux = array();

        foreach ($results as $key => $value) {
            $arr_alerta = $medicion->sanitizeAlerts($value->alert);

            $aux[$key] = $value;
            $aux[$key]['alert'] = json_encode($arr_alerta);
            $aux[$key]['isAlert'] = $medicion->isAlert($arr_alerta);
            $aux[$key]['countAlerts'] = $medicion->countAlert($arr_alerta);
        }

        $this->set('mediciones', $aux);
        $this->set('_serialize', ['mediciones']);
    }


    private $trad_months = array(1 =>  "Gener",
                                 2 => "Febrer",
                                 3 => "Març",
                                 4 => "Abril",
                                 5 => "Maig",
                                 6 => "Juny",
                                 7 => "Juliol",
                                 8 => "Agost",
                                 9 => "Setembre",
                                 10 => "Octubre",
                                 11 => "Novembre",
                                 12 => "Desembre", );

    public function getAllAlertasDateOrdered($id = null, $limit = 50)
    {
        Time::setJsonEncodeFormat('dd/MM/yyy HH:mm');

        $medicion = new Medicion();

        // Obtenemos todas las mediciones
        $query = $this->Medicion
            ->find('all')
            ->contain(['Local', 'Module'])
            ->where(['Medicion.local_id' => $id])
            ->limit($limit)
            ->order('Medicion.hour DESC');

        $results = $query->all();

        $aux = array();

        $order=0;


        foreach ($results as $key => $value) {
            $date = explode('/', substr($value['date'], 0, 8));
            if(intval($date[1])<10){ $date[1]="0".$date[1]; }
            if(intval($date[0])<10){ $date[0]="0".$date[0]; }
            $_key = str_replace(' ','',$date[2].$date[1].$date[0]);

            $arr_alerta = $medicion->sanitizeAlerts($value->alert);
            $isAlert = $medicion->isAlert($arr_alerta);

            if ( isset($isAlert) && $isAlert ) {
                $aux[$_key]['day']   = $date[0];
                $aux[$_key]['month'] = $this->trad_months[$date[1]];
                $aux[$_key]['year']  = trim($date[2]);
                $aux[$_key]['date'] = $value['date'];
                $aux[$_key][$key]['incidencias'] = $value;
                $aux[$_key][$key]['alert'] = $arr_alerta;
                $aux[$_key][$key]['isAlert'] = $medicion->isAlert($arr_alerta);
                $aux[$_key][$key]['countAlerts'] = $medicion->countAlert($arr_alerta);
            }
        }

        $newone= array();

        foreach($aux as $a){
            $newone[]=$a;

        }

        ksort($newone);

        $this->set('mediciones', $newone);
        $this->set('_serialize', ['mediciones']);
    }



    /**
     * Envia un email al Cliente del local con un pequeño informe
     * de la incidencia o alerta que ocurrió en la medición     *
     *
     * @return boolean
     * */
    public function sendEmail($alert = null, $local_id = null, $module_id = null, $fecha = null) {
        // testing data
        $this->autoRender = false;

        if ( !empty($alert) ) {
            $medicion = new Medicion();
            $arr_alerta = $medicion->sanitizeAlerts($alert);

            // si se produjo una alerta
            if ( $medicion->isAlert($arr_alerta) ) {
                // contador de Alertas
                $count_alerts = $medicion->countAlert($arr_alerta);

                $this->loadModel('Local');

                // datos del Local & Cliente
                $local = $this->Local
                    ->find('all')
                    ->where(['Local.id' => $local_id])
                    ->contain(['Client'])->first();

                // datos de la seccion medida
                $module = TableRegistry::get('Module')->find('list')->toArray();


                // enviamos email
                $email = new Email('default');
                $email->template('medicion', null)
                    ->emailFormat('html')
                    ->from(['app@laiflyadema.com' => 'Laiflyadema'])
                    ->to($local['client']['email'])
                    ->subject('[[Layiflyadema]] Medicións amb incidencies a: '.$local->name)
                    ->viewVars([
                        'local'        => $local,
                        'cliente'      => $local->cliente,
                        'module'       => $module,
                        'mediciones'     => $alert,
                        'fecha'     => $fecha,
                        'alert'        => $arr_alerta,
                        'count_alerts' => $count_alerts])
                    ->send();
            }
        }
    }


    /**
     * Envia un email mensual con el listado de mediciones de los ultimos 30 días
     *
     * @return boolean
     * */
    public function sendEmailMonthly() {
        $this->autoRender = false;

        $results = $this->getMedicionForIndex();
        $mediciones = $results['medicion'];

        $pdf_download = $this->download_pdf($mediciones);

        // enviamos email
        $email = new Email('default');
        $email->template('medicion_monthly', null)
            ->emailFormat('html')
            ->from(['app@laiflyadema.com' => 'Laiflyadema'])
            ->to('sergio@nukstudio.com')
            ->bcc('sergio@nukstudio.com')
            ->subject('Recopilación mensual de medicions')
            ->attachments($pdf_download)
            ->send();

    }



}
