<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Provider Controller
 *
 * @property \App\Model\Table\ProviderTable $Provider
 */
class ProviderController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');

        // allow all (testing)
        $this->Auth->allow();
    }


    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $session = $this->request->session()->read();

        $this->paginate = [
            'contain' => ['Client', 'Local']
        ];

        if ( isset($session['Auth']['User']['role']) && $session['Auth']['User']['role'] == 'user' ) {
            $this->set('provider', $this->paginate($this->Provider->find('all',
                                         ['conditions' => ['Provider.local_id' => $session['Auth']['User']['local_id']],])));
        }
        else {
            $this->set('provider', $this->paginate($this->Provider));
        }

        $this->set('_serialize', ['provider']);
    }

    /**
     * View method
     *
     * @param string|null $id Provider id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $provider = $this->Provider->get($id, [
            'contain' => ['Client', 'Local']
        ]);
        $this->set('provider', $provider);
        $this->set('_serialize', ['provider']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $provider = $this->Provider->newEntity();
        if ($this->request->is('post')) {
            $provider = $this->Provider->patchEntity($provider, $this->request->data);
            if ($this->Provider->save($provider)) {
                $this->Flash->success(__('The provider has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The provider could not be saved. Please, try again.'));
            }
        }

        $session = $this->request->session()->read('Auth.User');

        if ( isset($session['role']) && $session['role'] == 'user' ) {
            $client = $this->Provider->Client->find('list',['conditions' => ['Client.id' => $session['client_id']],'limit' => 1]);
            $local = $this->Provider->Local->find('list',['conditions' => ['Local.id' => $session['local_id']],'limit' => 1]);
        }else {
            $client = $this->Provider->Client->find('list', ['limit' => 200]);
            $local = $this->Provider->Local->find('list', ['limit' => 200]);
        }

        $this->set(compact('provider', 'client', 'local'));
        $this->set('_serialize', ['provider']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Provider id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $session = $this->request->session()->read();

        $provider = $this->Provider->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $provider = $this->Provider->patchEntity($provider, $this->request->data);
            if ($this->Provider->save($provider)) {
                $this->Flash->success(__('The provider has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The provider could not be saved. Please, try again.'));
            }
        }


        if ( isset($session['Auth']['User']['role']) && $session['Auth']['User']['role'] == 'user' ) {
            $client = $this->Provider->Client->find('list',
                ['conditions' => ['Client.id' => $session['Auth']['User']['client_id']],
                 'limit' => 1]);

            $local = $this->Provider->Local->find('list',
                ['conditions' => ['Local.id' => $session['Auth']['User']['local_id']],
                 'limit' => 1]);
        }
        else {
            $client = $this->Provider->Client->find('list', ['limit' => 200]);

            $local = $this->Provider->Local->find('list', ['limit' => 200]);
        }

        $this->set(compact('provider', 'client', 'local'));
        $this->set('_serialize', ['provider']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Provider id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $provider = $this->Provider->get($id);
        if ($this->Provider->delete($provider)) {
            $this->Flash->success(__('The provider has been deleted.'));
        } else {
            $this->Flash->error(__('The provider could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function getProviderByLocalId($id = null) {
        $provider = $this->Provider->find('all')
            ->where(['Provider.local_id' => $id]);
        // ->contain(['Client', 'Local']);

        $this->set('provider', $provider);
        $this->set('_serialize', ['provider']);
    }

}
