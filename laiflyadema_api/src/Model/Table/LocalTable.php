<?php
namespace App\Model\Table;

use App\Model\Entity\Local;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Local Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Client
 * @property \Cake\ORM\Association\HasMany $Provider
 * @property \Cake\ORM\Association\HasMany $User
 */
class LocalTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('local');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Client', [
            'foreignKey' => 'client_id'
        ]);
        $this->hasMany('Provider', [
            'foreignKey' => 'local_id'
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'local_id'
        ]);
        $this->hasMany('ModuleLocal', [
            'foreignKey' => 'local_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name');

        $validator
            ->allowEmpty('address');

        $validator
            ->allowEmpty('medicio1');

        $validator
            ->allowEmpty('medicio2');

        $validator
            ->allowEmpty('medicio3');

        $validator
            ->allowEmpty('medicio4');

        $validator
            ->allowEmpty('medicio5');

        $validator
            ->allowEmpty('medicio6');

        $validator
            ->allowEmpty('medicio7');

        $validator
            ->allowEmpty('medicio8');

        $validator
            ->allowEmpty('medicio9');

        $validator
            ->allowEmpty('medicio10');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['client_id'], 'Client'));
        return $rules;
    }
}
