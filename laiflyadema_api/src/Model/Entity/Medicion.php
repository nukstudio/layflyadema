<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Medicion Entity.
 *
 * @property int $id
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property int $local_id
 * @property \App\Model\Entity\Local $local
 * @property \Cake\I18n\Time $date
 * @property \Cake\I18n\Time $hour
 * @property int $module_id
 * @property \App\Model\Entity\Module $module
 * @property string $data
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Medicion extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];


    public function sanitizeAlerts($alert = null) {
        $response = array();

        if (!empty($alert)) {
            $response = $this->removeNullAlerts($alert);
        }

        return $response;
    }

    private function removeNullAlerts ($str) {
        $pos = strrpos($str, "null,{");

        if ($pos !== false) {
            $str = substr($str, $pos+5);
            $str = substr($str, 0, strlen($str)-1);
        }

        return json_decode($str, true);
    }



    public function isAlert($arr_alerts) {
        if ( !empty($arr_alerts) ) {
            foreach ($arr_alerts as $key => $value) {
                if ( $value['error'] === true )
                    return true;
            }
        }

        return false;
    }

    public function countAlert($arr_alerts) {
        $count = 0;

        if ( !empty($arr_alerts) ) {
            foreach ($arr_alerts as $key => $value) {
                if ( $value['error'] === true )
                    $count++;
            }
        }

        return $count;
    }
}
