<?php
    $session = $this->request->session()->read();
?>
<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <?php
            if ( isset($session['Auth']['User']['role']) && $session['Auth']['User']['role'] == 'user' ) {
        ?>
        <li><?= $this->Html->link('New Zona', ['action' => 'add']) ?></li>
        <li><?php echo $this->Html->link('Crear Subzona', ['controller' => 'subzona', 'action' => 'add']) ?></li>
        <?php
            }
            else {
        ?>
        <li><?= $this->Html->link(__('New Zona'), ['action' => 'add']) ?></li>
        <li><?php echo $this->Html->link('Crear Subzona', ['controller' => 'subzona', 'action' => 'add']) ?></li>
        <?php
            }
        ?>
    </ul>
</div>
<div class="zona index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('local_id') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th><?= $this->Paginator->sort('modified') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($zona as $zona): ?>
        <tr>
            <td><?= $this->Number->format($zona->id) ?></td>
            <td>
                <?= $zona->has('local') ? $this->Html->link($zona->local->name, ['controller' => 'Local', 'action' => 'view', $zona->local->id]) : '' ?>
            </td>
            <td><?= h($zona->name) ?></td>
            <td><?= h($zona->created) ?></td>
            <td><?= h($zona->modified) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $zona->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $zona->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $zona->id], ['confirm' => __('Are you sure you want to delete # {0}?', $zona->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
