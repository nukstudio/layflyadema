<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Zona'), ['action' => 'edit', $zona->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Zona'), ['action' => 'delete', $zona->id], ['confirm' => __('Are you sure you want to delete # {0}?', $zona->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Zona'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Zona'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Local'), ['controller' => 'Local', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Local'), ['controller' => 'Local', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="zona view large-10 medium-9 columns">
    <h2><?= h($zona->name) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Local') ?></h6>
            <p><?= $zona->has('local') ? $this->Html->link($zona->local->name, ['controller' => 'Local', 'action' => 'view', $zona->local->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Name') ?></h6>
            <p><?= h($zona->name) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($zona->id) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Created') ?></h6>
            <p><?= h($zona->created) ?></p>
            <h6 class="subheader"><?= __('Modified') ?></h6>
            <p><?= h($zona->modified) ?></p>
        </div>
    </div>
</div>

<div class="related">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Subzonas') ?></h4>
    <?php if (!empty($zona->subzona)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th width="80"><?= __('Id') ?></th>
            <th><?= __('Name') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($zona->subzona as $subzona): ?>
        <tr>
            <td><?= h($subzona->id) ?></td>
            <td><?= h($subzona->name) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'subzona', 'action' => 'view', $subzona->id]) ?>
                <?= $this->Html->link(__('Edit'), ['controller' => 'subzona', 'action' => 'edit', $subzona->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['controller' => 'subzona', 'action' => 'delete', $subzona->id], ['confirm' => __('Are you sure you want to delete # {0}?', $subzona->id)]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
