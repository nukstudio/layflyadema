<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Elimina la Zona'),
                ['action' => 'delete', $zona->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $zona->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Tornar a Zones'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="zona form large-10 medium-9 columns">
    <?= $this->Form->create($zona) ?>
    <fieldset>
        <legend><?= __('Edit Zona') ?></legend>
        <?php
            echo $this->Form->input('local_id', ['options' => $local]);
            echo $this->Form->input('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
