<?php
    $session = $this->request->session()->read();
?>
<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <?php
            if ( isset($session['Auth']['User']['role']) && $session['Auth']['User']['role'] == 'user' ) {
        ?>
        <li><?= $this->Html->link(__('New Equipo'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Local'), ['controller' => 'Local', 'action' => 'index']) ?></li>
        <?php
            }
            else {
        ?>
        <li><?= $this->Html->link(__('New Equipo'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Local'), ['controller' => 'Local', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Local'), ['controller' => 'Local', 'action' => 'add']) ?></li>
        <?php
            }
        ?>
    </ul>
</div>
<div class="equipo index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('local_id') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('max') ?></th>
            <th><?= $this->Paginator->sort('min') ?></th>
            <th><?= $this->Paginator->sort('standard') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($equipo as $equipo): ?>
        <tr>
            <td><?= $this->Number->format($equipo->id) ?></td>
            <td>
                <?= $equipo->has('local') ? $this->Html->link($equipo->local->name, ['controller' => 'Local', 'action' => 'view', $equipo->local->id]) : '' ?>
            </td>
            <td><?= h($equipo->name) ?></td>
            <td><?= $this->Number->format($equipo->max) ?></td>
            <td><?= $this->Number->format($equipo->min) ?></td>
            <td><?= $this->Number->format($equipo->standard) ?></td>
            <td><?= h($equipo->created) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $equipo->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $equipo->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $equipo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $equipo->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
