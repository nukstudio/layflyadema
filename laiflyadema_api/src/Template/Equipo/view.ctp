<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Equipo'), ['action' => 'edit', $equipo->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Equipo'), ['action' => 'delete', $equipo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $equipo->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Equipo'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Equipo'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Local'), ['controller' => 'Local', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Local'), ['controller' => 'Local', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="equipo view large-10 medium-9 columns">
    <h2><?= h($equipo->name) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Local') ?></h6>
            <p><?= $equipo->has('local') ? $this->Html->link($equipo->local->name, ['controller' => 'Local', 'action' => 'view', $equipo->local->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Name') ?></h6>
            <p><?= h($equipo->name) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($equipo->id) ?></p>
            <h6 class="subheader"><?= __('Max') ?></h6>
            <p><?= $this->Number->format($equipo->max) ?></p>
            <h6 class="subheader"><?= __('Min') ?></h6>
            <p><?= $this->Number->format($equipo->min) ?></p>
            <h6 class="subheader"><?= __('Standard') ?></h6>
            <p><?= $this->Number->format($equipo->standard) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Created') ?></h6>
            <p><?= h($equipo->created) ?></p>
            <h6 class="subheader"><?= __('Modified') ?></h6>
            <p><?= h($equipo->modified) ?></p>
        </div>
    </div>
</div>
