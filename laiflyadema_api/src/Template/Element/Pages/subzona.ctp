<div class="actions columns large-4">
    <h3><?php echo __('Subzones de neteja') ?></h3>
    <p class="text">Configuració de les Subzones. <br /></p>
    <ul class="side-nav">
        <li><?php echo $this->Html->link(__('Visualitzar tots'), ['controller' => 'subzona', 'action' => 'index']) ?> </li>
        <li><?php echo $this->Html->link(__('Insertar'), ['controller' => 'subzona', 'action' => 'add']) ?> </li>
    </ul>
</div>
