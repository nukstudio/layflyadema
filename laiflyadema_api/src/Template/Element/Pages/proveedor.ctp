<div class="actions columns large-4">
    <h3><?php echo __('Proveedores') ?></h3>
    <p class="text">Configuració de Proveïdors. <br />Els proveïdors es defineixen per cada Local.</p>
    <ul class="side-nav">
        <li><?php echo $this->Html->link(__('Visualitzar tots'), ['controller' => 'provider', 'action' => 'index']) ?> </li>
        <li><?php echo $this->Html->link(__('Insertar'), ['controller' => 'provider', 'action' => 'add']) ?> </li>
    </ul>
</div>
