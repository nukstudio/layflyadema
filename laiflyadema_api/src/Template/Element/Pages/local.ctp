<div class="actions columns large-4">
    <h3><?php echo __('Locales') ?></h3>
    <p class="text">Configuració dels Locals. <br />Un local sempre pertany a un Client.</p>
    <ul class="side-nav">
        <li><?php echo $this->Html->link(__('Visualitzar tots'), ['controller' => 'local', 'action' => 'index']) ?> </li>
        <li><?php echo $this->Html->link(__('Insertar'), ['controller' => 'local', 'action' => 'add']) ?> </li>
    </ul>
</div>
