<div class="actions columns large-4">
    <h3><?php echo __('Usuarios') ?></h3>
    <p class="text">Configuració dels Usuaris. <br />Els usuaris s'asocien a un local.</p>
    <ul class="side-nav">
        <li><?php echo $this->Html->link(__('Visualitzar tots'), ['controller' => 'users', 'action' => 'index']) ?> </li>
        <li><?php echo $this->Html->link(__('Insertar'), ['controller' => 'users', 'action' => 'add']) ?> </li>
    </ul>
</div>
