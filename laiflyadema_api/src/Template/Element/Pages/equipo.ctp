<div class="actions columns large-4">
    <h3><?php echo __('Equips') ?></h3>
    <p class="text">Configuració dels Equips de refrigeració. <br />Els equips es defineixen per cada Local en concret.</p>
    <ul class="side-nav">
        <li><?php echo $this->Html->link(__('Visualitzar tots'), ['controller' => 'equipo', 'action' => 'index']) ?> </li>
        <li><?php echo $this->Html->link(__('Insertar'), ['controller' => 'equipo', 'action' => 'add']) ?> </li>
    </ul>
</div>
