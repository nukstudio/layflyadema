<div class="actions columns large-4">
    <h3><?php echo __('Zones de neteja') ?></h3>
    <p class="text">Configuració de les Zones. <br />Les zones es defineixen per cada Local en concret.</p>
    <ul class="side-nav">
        <li><?php echo $this->Html->link(__('Visualitzar tots'), ['controller' => 'zona', 'action' => 'index']) ?> </li>
        <li><?php echo $this->Html->link(__('Insertar'), ['controller' => 'zona', 'action' => 'add']) ?> </li>
    </ul>
</div>
