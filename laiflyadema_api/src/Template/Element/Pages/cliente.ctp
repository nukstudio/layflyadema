<div class="actions columns large-4">
    <h3><?php echo __('Clientes') ?></h3>
    <p class="text">Definició dels Clients/Empreses físiques que actualment estàm utilitzant l'APP.</p>
    <ul class="side-nav">
        <li><?php echo $this->Html->link(__('Visualitzar tots'), ['controller' => 'client', 'action' => 'index']) ?> </li>
        <li><?php echo $this->Html->link(__('Insertar'), ['controller' => 'client', 'action' => 'add']) ?> </li>
    </ul>
</div>
