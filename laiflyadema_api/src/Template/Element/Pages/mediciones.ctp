<div class="actions columns large-4">
    <h3><?php echo __('Medicions') ?></h3>
    <p class="text">Medicions enviades pels usuaris</p>
    <ul class="side-nav">
        <li><?php echo $this->Html->link(__('Visualitzar'), ['controller' => 'medicion', 'action' => 'index']) ?> </li>
    </ul>
</div>
