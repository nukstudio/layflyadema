<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Elimina la Subzona'),
                ['action' => 'delete', $subzona->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $subzona->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Tornar a Subzones'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="subzona form large-10 medium-9 columns">
    <?= $this->Form->create($subzona) ?>
    <fieldset>
        <legend><?= __('Edit Subzona') ?></legend>
        <?php
            echo $this->Form->input('zona_id');
            echo $this->Form->input('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
