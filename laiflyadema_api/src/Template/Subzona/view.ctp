<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Subzona'), ['action' => 'edit', $subzona->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Subzona'), ['action' => 'delete', $subzona->id], ['confirm' => __('Are you sure you want to delete # {0}?', $subzona->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Subzona'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Subzona'), ['action' => 'add']) ?> </li>
    </ul>
</div>
<div class="subzona view large-10 medium-9 columns">
    <h2><?= h($subzona->name) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Name') ?></h6>
            <p><?= h($subzona->name) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($subzona->id) ?></p>
            <h6 class="subheader"><?= __('Zona Id') ?></h6>
            <p><?= $this->Number->format($subzona->zona_id) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Created') ?></h6>
            <p><?= h($subzona->created) ?></p>
            <h6 class="subheader"><?= __('Modified') ?></h6>
            <p><?= h($subzona->modified) ?></p>
        </div>
    </div>
</div>
