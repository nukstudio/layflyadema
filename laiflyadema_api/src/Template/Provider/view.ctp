<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <?php
            $session = $this->request->session()->read();
            if ( isset($session['Auth']['User']['role']) && $session['Auth']['User']['role'] == 'user' ) {
        ?>
        <li><?= $this->Html->link(__('New Provider'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Client'), ['controller' => 'Client', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Local'), ['controller' => 'Local', 'action' => 'index']) ?></li>
        <?php
            }
            else {
        ?>
        <li><?= $this->Html->link(__('New Provider'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Client'), ['controller' => 'Client', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Client'), ['controller' => 'Client', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Local'), ['controller' => 'Local', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Local'), ['controller' => 'Local', 'action' => 'add']) ?></li>
        <?php
            }
        ?>
    </ul>
</div>
<div class="provider view large-10 medium-9 columns">
    <h2><?= h($provider->name) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Client') ?></h6>
            <?php
                if ( isset($session['Auth']['User']['role']) && $session['Auth']['User']['role'] == 'user' ) {
            ?>
            <p><?php echo $provider->client->name; ?></p>
            <?php
                }
                else {
            ?>
            <p><?= $provider->has('client') ? $this->Html->link($provider->client->name, ['controller' => 'Client', 'action' => 'view', $provider->client->id]) : '' ?></p>
            <?php
                }
            ?>
            <h6 class="subheader"><?= __('Local') ?></h6>
            <p><?= $provider->has('local') ? $this->Html->link($provider->local->name, ['controller' => 'Local', 'action' => 'view', $provider->local->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Name') ?></h6>
            <p><?= h($provider->name) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($provider->id) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Created') ?></h6>
            <p><?= h($provider->created) ?></p>
            <h6 class="subheader"><?= __('Modified') ?></h6>
            <p><?= h($provider->modified) ?></p>
        </div>
        <div class="large-2 columns booleans end">
            <h6 class="subheader"><?= __('Congelados') ?></h6>
            <p><?= $provider->congelados ? __('Yes') : __('No'); ?></p>
            <h6 class="subheader"><?= __('Carne') ?></h6>
            <p><?= $provider->carne ? __('Yes') : __('No'); ?></p>
            <h6 class="subheader"><?= __('Pescado') ?></h6>
            <p><?= $provider->pescado ? __('Yes') : __('No'); ?></p>
            <h6 class="subheader"><?= __('Fruta') ?></h6>
            <p><?= $provider->fruta ? __('Yes') : __('No'); ?></p>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Description') ?></h6>
            <?= $this->Text->autoParagraph(h($provider->description)) ?>
        </div>
    </div>
</div>
