<?php
    $session = $this->request->session()->read();
?>
<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <?php
            if ( isset($session['Auth']['User']['role']) && $session['Auth']['User']['role'] == 'user' ) {
        ?>
        <li><?= $this->Html->link(__('New Provider'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Client'), ['controller' => 'Client', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Local'), ['controller' => 'Local', 'action' => 'index']) ?></li>
        <?php
            }
            else {
        ?>
        <li><?= $this->Html->link(__('New Provider'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Client'), ['controller' => 'Client', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Client'), ['controller' => 'Client', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Local'), ['controller' => 'Local', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Local'), ['controller' => 'Local', 'action' => 'add']) ?></li>
        <?php
            }
        ?>
    </ul>
</div>
<div class="provider form large-10 medium-9 columns">
    <?= $this->Form->create($provider) ?>
    <fieldset>
        <legend><?= __('Add Provider') ?></legend>
        <?php
            echo $this->Form->input('client_id', ['options' => $client, 'empty' => true]);
            echo $this->Form->input('local_id', ['options' => $local]);
            echo $this->Form->input('name');
            echo $this->Form->input('description');
            echo $this->Form->input('congelados');
            echo $this->Form->input('carne');
            echo $this->Form->input('pescado');
            echo $this->Form->input('fruta');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
