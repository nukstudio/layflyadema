<?php
    $session = $this->request->session()->read();
?>
<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <?php
            if ( isset($session['Auth']['User']['role']) && $session['Auth']['User']['role'] == 'user' ) {
        ?>
        <li><?= $this->Html->link(__('New Provider'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Client'), ['controller' => 'Client', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Local'), ['controller' => 'Local', 'action' => 'index']) ?></li>
        <?php
            }
            else {
        ?>
        <li><?= $this->Html->link(__('New Provider'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Client'), ['controller' => 'Client', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Client'), ['controller' => 'Client', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Local'), ['controller' => 'Local', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Local'), ['controller' => 'Local', 'action' => 'add']) ?></li>
        <?php
            }
        ?>
    </ul>
</div>
<div class="provider index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('local_id') ?></th>
            <th width="80"><?= $this->Paginator->sort('congelados') ?></th>
            <th width="80"><?= $this->Paginator->sort('carne') ?></th>
            <th width="80"><?= $this->Paginator->sort('pescado') ?></th>
            <th width="80"><?= $this->Paginator->sort('fruta') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($provider as $provider): ?>
        <tr>
            <?php
                $session = $this->request->session()->read();
            ?>
            <td><?= $provider->name; ?></td>
            <td>
                <?= $provider->has('local') ? $this->Html->link($provider->local->name, ['controller' => 'Local', 'action' => 'view', $provider->local->id]) : '' ?>
            </td>
            <td><?= $provider->congelados ? __('Yes') : __('No'); ?></td>
            <td><?= $provider->carne ? __('Yes') : __('No'); ?></td>
            <td><?= $provider->pescado ? __('Yes') : __('No'); ?></td>
            <td><?= $provider->fruta ? __('Yes') : __('No'); ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $provider->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $provider->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $provider->id], ['confirm' => __('Are you sure you want to delete # {0}?', $provider->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
