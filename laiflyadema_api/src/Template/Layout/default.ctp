<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Laiflyadema backoffice';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
      $(function() {
        $("#date_from").datepicker({
            dateFormat: 'dd/mm/yy',
            onSelect: function(date){
                var date = new Date(Date.parse($(this).datepicker('getDate')));
                date.setDate(date.getDate());
                var newDate = date.toDateString();
                newDate = new Date(Date.parse( newDate ));

                $('#date_to').datepicker("option", "minDate", newDate);
            }
        });
        $("#date_to").datepicker({
            dateFormat: 'dd/mm/yy',
        });
      });
    </script>
</head>
<body>
    <header>
        <div class="header-title">
            <a href="<?php echo $this->request->webroot; ?>"><img class="logo" width="48" src="<?php echo $this->request->webroot; ?>img/topbar-logo.jpg" /></a>
            <?php if($this->request->session()->read('Auth.User')){ ?>
            <ul style="list-style:none; display:inline;">
                <?php if($this->request->session()->read('Auth.User.role')=='admin'){ ?>
                <li style="display:inline;"><?php echo $this->Html->link(__('Clients'), ['controller' => 'client', 'action' => 'index']) ?></li>
                <li style="display:inline;">|</li>
                <li style="display:inline;"><?php echo $this->Html->link(__('Usuaris'), ['controller' => 'users', 'action' => 'index']) ?></li>
                <li style="display:inline;">|</li>
                <?php } ?>
                <li style="display:inline;"><?php echo $this->Html->link(__('Locals'), ['controller' => 'local', 'action' => 'index']) ?></li>
                <li style="display:inline;">|</li>
                <li style="display:inline;"><?php echo $this->Html->link(__('Equips'), ['controller' => 'equipo', 'action' => 'index']) ?></li>
                <li style="display:inline;">|</li>
                <li style="display:inline;"><?php echo $this->Html->link(__('Proveïdors'), ['controller' => 'provider', 'action' => 'index']) ?></li>
                <li style="display:inline;">|</li>
                <li style="display:inline;"><?php echo $this->Html->link(__('Zones de neteja'), ['controller' => 'zona', 'action' => 'index']) ?></li>
                <li style="display:inline;">|</li>
                <li style="display:inline;"><?php echo $this->Html->link(__('Medicions'), ['controller' => 'medicion', 'action' => 'index']) ?></li>
            </ul>
            <?php } ?>
        </div>
        <div class="header-help">
            <?php
                $user_id = $this->request->session()->read('Auth.User.id');

                if ( isset($user_id) ) {
            ?>
            <span><a href="<?php echo $this->request->webroot; ?>users/logout">Desconectar</a></span>
            <?php
                }
            ?>
        </div>
    </header>
    <div id="container">

        <div id="content">

            <div class="row">
                <?= $this->fetch('content') ?>
            </div>
        </div>
        <footer>
        </footer>
    </div>
</body>
</html>
