<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Module Local'), ['action' => 'edit', $moduleLocal->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Module Local'), ['action' => 'delete', $moduleLocal->id], ['confirm' => __('Are you sure you want to delete # {0}?', $moduleLocal->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Module Local'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Module Local'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Module'), ['controller' => 'Module', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Module'), ['controller' => 'Module', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Local'), ['controller' => 'Local', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Local'), ['controller' => 'Local', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="moduleLocal view large-10 medium-9 columns">
    <h2><?= h($moduleLocal->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Module') ?></h6>
            <p><?= $moduleLocal->has('module') ? $this->Html->link($moduleLocal->module->name, ['controller' => 'Module', 'action' => 'view', $moduleLocal->module->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Local') ?></h6>
            <p><?= $moduleLocal->has('local') ? $this->Html->link($moduleLocal->local->name, ['controller' => 'Local', 'action' => 'view', $moduleLocal->local->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($moduleLocal->id) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Created') ?></h6>
            <p><?= h($moduleLocal->created) ?></p>
            <h6 class="subheader"><?= __('Modified') ?></h6>
            <p><?= h($moduleLocal->modified) ?></p>
        </div>
    </div>
</div>
