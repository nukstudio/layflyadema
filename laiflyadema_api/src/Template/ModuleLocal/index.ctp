<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Module Local'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Module'), ['controller' => 'Module', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Module'), ['controller' => 'Module', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Local'), ['controller' => 'Local', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Local'), ['controller' => 'Local', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="moduleLocal index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('module_id') ?></th>
            <th><?= $this->Paginator->sort('local_id') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th><?= $this->Paginator->sort('modified') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($moduleLocal as $moduleLocal): ?>
        <tr>
            <td><?= $this->Number->format($moduleLocal->id) ?></td>
            <td>
                <?= $moduleLocal->has('module') ? $this->Html->link($moduleLocal->module->name, ['controller' => 'Module', 'action' => 'view', $moduleLocal->module->id]) : '' ?>
            </td>
            <td>
                <?= $moduleLocal->has('local') ? $this->Html->link($moduleLocal->local->name, ['controller' => 'Local', 'action' => 'view', $moduleLocal->local->id]) : '' ?>
            </td>
            <td><?= h($moduleLocal->created) ?></td>
            <td><?= h($moduleLocal->modified) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $moduleLocal->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $moduleLocal->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $moduleLocal->id], ['confirm' => __('Are you sure you want to delete # {0}?', $moduleLocal->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
