<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Client'), ['action' => 'edit', $client->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Client'), ['action' => 'delete', $client->id], ['confirm' => __('Are you sure you want to delete # {0}?', $client->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Client'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Client'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Local'), ['controller' => 'Local', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Local'), ['controller' => 'Local', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Provider'), ['controller' => 'Provider', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Provider'), ['controller' => 'Provider', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="client view large-10 medium-9 columns">
    <h2><?= h($client->name) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Name') ?></h6>
            <p><?= h($client->name) ?></p>
            <h6 class="subheader"><?= __('Cif') ?></h6>
            <p><?= h($client->cif) ?></p>
            <h6 class="subheader"><?= __('Email') ?></h6>
            <p><?= h($client->email) ?></p>
            <h6 class="subheader"><?= __('Phone') ?></h6>
            <p><?= h($client->phone) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($client->id) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Created') ?></h6>
            <p><?= h($client->created) ?></p>
            <h6 class="subheader"><?= __('Modified') ?></h6>
            <p><?= h($client->modified) ?></p>
        </div>
        <div class="large-2 columns booleans end">
            <h6 class="subheader"><?= __('Enabled') ?></h6>
            <p><?= $client->enabled ? __('Yes') : __('No'); ?></p>
        </div>
    </div>
</div>
<div class="related">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Local') ?></h4>
    <?php if (!empty($client->local)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Client Id') ?></th>
            <th><?= __('Name') ?></th>
            <th><?= __('Address') ?></th>
            <th><?= __('Created') ?></th>
            <th><?= __('Modified') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($client->local as $local): ?>
        <tr>
            <td><?= h($local->id) ?></td>
            <td><?= h($local->client_id) ?></td>
            <td><?= h($local->name) ?></td>
            <td><?= h($local->address) ?></td>
            <td><?= h($local->created) ?></td>
            <td><?= h($local->modified) ?></td>

            <td class="actions">

                <?= $this->Html->link(__('View'), ['controller' => 'Local', 'action' => 'view', $local->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Local', 'action' => 'edit', $local->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Local', 'action' => 'delete', $local->id], ['confirm' => __('Are you sure you want to delete # {0}?', $local->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<div class="related">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Provider') ?></h4>
    <?php if (!empty($client->provider)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Client Id') ?></th>
            <th><?= __('Local Id') ?></th>
            <th><?= __('Name') ?></th>
            <th><?= __('Description') ?></th>
            <th><?= __('Created') ?></th>
            <th><?= __('Modified') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($client->provider as $provider): ?>
        <tr>
            <td><?= h($provider->id) ?></td>
            <td><?= h($provider->client_id) ?></td>
            <td><?= h($provider->local_id) ?></td>
            <td><?= h($provider->name) ?></td>
            <td><?= h($provider->description) ?></td>
            <td><?= h($provider->created) ?></td>
            <td><?= h($provider->modified) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Provider', 'action' => 'view', $provider->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Provider', 'action' => 'edit', $provider->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Provider', 'action' => 'delete', $provider->id], ['confirm' => __('Are you sure you want to delete # {0}?', $provider->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
