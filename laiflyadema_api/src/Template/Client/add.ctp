<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Tornar a Clients'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="client form large-10 medium-9 columns">
    <?= $this->Form->create($client) ?>
    <fieldset>
        <legend><?= __('Add Client') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('cif');
            echo $this->Form->input('email');
            echo $this->Form->input('phone');
            echo $this->Form->input('enabled');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
