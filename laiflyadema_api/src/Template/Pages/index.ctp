<?php
    $user_role = $this->request->session()->read('Auth.User.role');

    if ( isset($user_role) ) {

        switch ($user_role) {
            case 'admin':
                echo '<div class="row">';
                    echo $this->element('Pages/cliente');
                    echo $this->element('Pages/local');
                    echo $this->element('Pages/usuario');
                echo '</div>';

                echo $this->element('Pages/separator');

                echo '<div class="row">';
                    echo $this->element('Pages/proveedor');
                    echo $this->element('Pages/equipo');
                    echo $this->element('Pages/zona');
                echo '</div>';

                echo $this->element('Pages/separator');

                echo '<div class="row">';
                    echo $this->element('Pages/mediciones');
                echo '</div>';
                break;

            case 'user':
                echo '<div class="row">';
                    echo $this->element('Pages/local');
                    echo $this->element('Pages/equipo');
                    echo $this->element('Pages/proveedor');
                echo '</div>';

                echo $this->element('Pages/separator');

                echo '<div class="row">';
                    echo $this->element('Pages/zona');
                    echo $this->element('Pages/subzona');
                    echo $this->element('Pages/mediciones');
                echo '</div>';
                break;

            default:
                # code...
                break;
        }
    }

?>
