<div class="medicion index large-12 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= __('Data') ?></th>
            <th><?= __('Local') ?></th>
            <th><?= __('Secció') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php
        foreach ($aux as $medicion) {
                $local = $medicion->local;
                $data = json_decode($medicion->data);
                $alerts = json_decode($medicion->alert);
                $alerts = $alerts[$medicion->module->id];
                foreach($alerts as $w=>$a){ 
                    if(!is_object($a)){ 
                        unset($alerts[$w]);
                    }else{
                        if($a->error!=1){
                            unset($alerts->$w);
                        }
                    }
                }
                $howMantAlerts = count((array)$alerts);
    ?>
        <tr style="border: 1px solid #C2C2C2; background:#0097a7;">
            <td style="color:#fff; font-weight:bold;"><?= h($medicion->hour) ?></td>
            <td style="color:#fff; font-weight:bold;"><?php echo $medicion->local->name; ?></td>
            <td style="color:#fff; font-weight:bold;"><?php echo $medicion->module->name; ?></td>
        </tr>
        <tr style="padding-bottom: 10px; border-bottom: 1px solid #C2C2C2; border-left: 1px solid #C2C2C2; border-right: 1px solid #C2C2C2; border-color: #C2C2C2;">
            <td colspan="3" style="padding-bottom: 20px;">
                <strong>Medició <?php echo $medicion->module->id; ?>:</strong>
                <ul style="margin-bottom: 10px;">
                    <?php 


                    if($medicion->module->id<=2){

                        echo '<strong>Clor:</strong>: '.($data->valor/100).'ppm';

                    }else if($medicion->module->id==10){

                        // LIMPIEZA

                        foreach($zonas as $z){
                            
                            if($z->id==$data->zona){
                                
                                if($howMantAlerts==0){
                                    
                                    echo '<strong>Zona neta:</strong> '.$z->name;
                                    echo '<br/><strong>Subzones:</strong><br/>';

                                }else{
                                    
                                    echo '<strong>Zona bruta:</strong> '.$z->name;
                                    echo '<br/><strong>Subzones:</strong><br/>';

                                }

                                echo '<ul>';

                                foreach($data->subzone as $key=>$sz){
                                    foreach($z->subzona as $s){
                                        if($key==$s->id){
                                            echo '<li>'.$s->name.'</li>';
                                        }
                                    }
                                }

                                echo '</ul>';

                            }
                        }

                    }else if($medicion->module->id==3){

                        foreach($data->temp as $d){

                            if(isset($d->name)){
                                echo "<strong>Equip:</strong> ".$d->name.' ';
                            }

                            echo "<strong>Temperatura</strong>: ".$d->valor.' ºC<br/>';

                        }

                    }else{
                        
                        foreach($data as $w=>$a){
                            if($a!=''){
                                if(is_array($a)){

                                    echo '<ul>';
                                    
                                    foreach($a as $c=>$d){
                                        
                                        echo '<li style="display:inline; float:left; margin-right:20px;">';

                                        if(is_object($d)){
                                            echo '<strong>'.$c.'</strong>:'.$d->valor;
                                        }else{
                                            echo '<strong>'.$c.'</strong>:'.$d;
                                        }
                                        echo '</li>';
                                    }

                                    echo '</ul>';

                                }else{

                                    echo '<li style="display:inline; float:left; margin-right:20px;">';
                                    echo '<strong>'.$w.'</strong>:';
                                    print_r($a);
                                    echo '</li>';

                                }
                                
                            }
                        }
                    }

                    ?>
                </ul>
            </td>
        </tr>
        <tr style="padding-bottom: 10px; border-bottom: 1px solid #C2C2C2; border-left: 1px solid #C2C2C2; border-right: 1px solid #C2C2C2; border-color: #C2C2C2;">
            <td colspan="3" style="padding-bottom: 20px;">
                <strong>Alertes: 
                    <span style="color:#ff0000;"><?php echo $howMantAlerts;?></span>
                </strong>
                <ul style="margin-bottom: 10px;">
            <?php

                if($howMantAlerts!=0){
                    foreach($alerts as $alert) {
                        if (  !empty($alert->msg) ) {
                            echo '<li style="font-size: 13px; padding-top: 10px; padding-bottom: 0px;">';
                            echo @$alert->title;
                            echo @$alert->msg;
                            echo '</li>';
                        }
                    }
                }else{ ?> <p>No s'han detectat incidéncies.</p> <?php 

                }
            ?>
                </ul>
            </td>
        </tr>
    <?php
            
        }
    ?>
    </tbody>
    </table>
</div>
