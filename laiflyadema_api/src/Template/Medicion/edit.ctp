<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $medicion->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $medicion->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Medicion'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="medicion form large-10 medium-9 columns">
    <?= $this->Form->create($medicion) ?>
    <fieldset>
        <legend><?= __('Edit Medicion') ?></legend>
        <?php
            echo $this->Form->input('user_id', ['options' => $users, 'empty' => true]);
            echo $this->Form->input('local_id');
            echo $this->Form->input('date');
            echo $this->Form->input('hour');
            echo $this->Form->input('module_id');
            echo $this->Form->input('data');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
