<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Elimina Local'),
                ['action' => 'delete', $local->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $local->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Tornar a Locals'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="local form large-10 medium-9 columns">
    <?= $this->Form->create($local) ?>
    <fieldset>
        <legend><?= __('Edit Local') ?></legend>
        <?php
            echo $this->Form->input('client_id', ['options' => $client, 'empty' => true]);
            echo $this->Form->input('name');
            echo $this->Form->input('address');
            echo $this->Form->input('medicio1',['type'=>'checkbox','label'=>'CONTROL CLOR RESIDUAL AIGUA DE CONSUM']);
            echo $this->Form->input('medicio2',['type'=>'checkbox','label'=>'CONTROL DE LA NETEJA I DESINFECCIÓ DE LA FRUITA I VERDURA DE CONSUM EN CRU']);
            echo $this->Form->input('medicio3',['type'=>'checkbox','label'=>'CONTROL TEMPERATURES DELS EQUIPS DE FRED']);
            echo $this->Form->input('medicio4',['type'=>'checkbox','label'=>'CONTROL CRITERIS EMMAGATZEMATGE ALIMENTS DINS DELS EQUIPS DE FRED']);
            echo $this->Form->input('medicio5',['type'=>'checkbox','label'=>'CONTROL DEL OLI DE LES FREGIDORES']);
            echo $this->Form->input('medicio6',['type'=>'checkbox','label'=>'CONTROL DE LA TEMPERATURA D\'ESBANDIT DE L\'EQUIP DE RENTAPLATS']);
            echo $this->Form->input('medicio7',['type'=>'checkbox','label'=>'CONTROL RECEPCIÓ MERCADERIA']);
            echo $this->Form->input('medicio8',['type'=>'checkbox','label'=>'CONTROL DE REFREDAMENT DE PLATS PREPARATS']);
            echo $this->Form->input('medicio9',['type'=>'checkbox','label'=>'CONTROL DE ENVASAMENT AL BUIT']);
            echo $this->Form->input('medicio10',['type'=>'checkbox','label'=>'CONTROL DE NETEJA']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
