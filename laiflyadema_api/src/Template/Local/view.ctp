<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <?php
            $session = $this->request->session()->read();

            if ( isset($session['Auth']['User']['role']) && $session['Auth']['User']['role'] == 'user' ) {
        ?>
        <li><?= $this->Html->link(__('List Client'), ['controller' => 'Client', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Provider'), ['controller' => 'Provider', 'action' => 'index']) ?></li>
        <?php
            }
            else {
        ?>
        <li><?= $this->Html->link(__('Edit Local'), ['action' => 'edit', $local->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Local'), ['action' => 'delete', $local->id], ['confirm' => __('Are you sure you want to delete # {0}?', $local->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Local'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Local'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Client'), ['controller' => 'Client', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Client'), ['controller' => 'Client', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Provider'), ['controller' => 'Provider', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Provider'), ['controller' => 'Provider', 'action' => 'add']) ?> </li>
        <?php
            }
        ?>
    </ul>
</div>
<div class="local view large-10 medium-9 columns">
    <h2><?= h($local->name) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Client') ?></h6>
            <?php
                $session = $this->request->session()->read();

                if ( isset($session['Auth']['User']['role']) && $session['Auth']['User']['role'] == 'user' ) {
                    echo '<p>'.$local->client->name.'</p>';
                }
                else {
                    echo '<p>'.$local->has('client') ? $this->Html->link($local->client->name, ['controller' => 'Client', 'action' => 'view', $local->client->id]) : ''.'</p>';
                }
            ?>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($local->id) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Created') ?></h6>
            <p><?= h($local->created) ?></p>
            <h6 class="subheader"><?= __('Modified') ?></h6>
            <p><?= h($local->modified) ?></p>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Name') ?></h6>
            <?= $this->Text->autoParagraph(h($local->name)) ?>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Address') ?></h6>
            <?= $this->Text->autoParagraph(h($local->address)) ?>
        </div>
    </div>
</div>
<div class="related">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Provider') ?></h4>
    <?php if (!empty($local->provider)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Client Id') ?></th>
            <th><?= __('Local Id') ?></th>
            <th><?= __('Name') ?></th>
            <th><?= __('Description') ?></th>
            <th><?= __('Created') ?></th>
            <th><?= __('Modified') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($local->provider as $provider): ?>
        <tr>
            <td><?= h($provider->id) ?></td>
            <td><?= h($provider->client_id) ?></td>
            <td><?= h($provider->local_id) ?></td>
            <td><?= h($provider->name) ?></td>
            <td><?= h($provider->description) ?></td>
            <td><?= h($provider->created) ?></td>
            <td><?= h($provider->modified) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Provider', 'action' => 'view', $provider->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Provider', 'action' => 'edit', $provider->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Provider', 'action' => 'delete', $provider->id], ['confirm' => __('Are you sure you want to delete # {0}?', $provider->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
