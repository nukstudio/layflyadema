<?php
    $session = $this->request->session()->read();
?>
<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <?php
            if ( isset($session['Auth']['User']['role']) && $session['Auth']['User']['role'] == 'user' ) {
        ?>
        <li><?= $this->Html->link(__('List Client'), ['controller' => 'Client', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Provider'), ['controller' => 'Provider', 'action' => 'index']) ?></li>
        <?php
            }
            else {
        ?>
        <li><?= $this->Html->link(__('New Local'), ['action' => 'add']) ?></li>
        <?php
            }
        ?>
    </ul>
</div>
<div class="local index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th width="50"><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('client') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($local as $local): ?>
        <tr>
            <td><?= $this->Number->format($local->id) ?></td>
            <td><?= $this->Html->link($local->name, ['controller' => 'Local', 'action' => 'view', $local->id]); ?></td>
            <?php
                $session = $this->request->session()->read();

                if ( isset($session['Auth']['User']['role']) && $session['Auth']['User']['role'] == 'user' ) {
                    echo '<td>'.$local->client->name.'</td>';
                }
                else {
                    echo '<td>'.$this->Html->link($local->client->name, ['controller' => 'Client', 'action' => 'view', $local->client->id]).'</td>';
                }
            ?>
            <td><?= h($local->created) ?></td>
            <td class="actions">
            <?php
                if ( isset($session['Auth']['User']['role']) && $session['Auth']['User']['role'] == 'user' ) {
            ?>
                <?= $this->Html->link(__('View'), ['action' => 'view', $local->id]) ?>
            <?php
                }
                else {
            ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $local->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $local->id], ['confirm' => __('Are you sure you want to delete # {0}?', $local->id)]) ?>
            <?php
                }
            ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
