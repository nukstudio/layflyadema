<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Tornar a Locals'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="local form large-10 medium-9 columns">
    <?= $this->Form->create($local) ?>
    <fieldset>
        <legend><?= __('Add Local') ?></legend>
        <?php
            echo $this->Form->input('client_id', ['options' => $client, 'empty' => true]);
            echo $this->Form->input('name');
            echo $this->Form->input('address');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
