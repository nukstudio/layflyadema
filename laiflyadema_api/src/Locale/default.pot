# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2015-09-18 10:10+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: Controller/ClientController.php:67;92
msgid "The client has been saved."
msgstr ""

#: Controller/ClientController.php:70;95
msgid "The client could not be saved. Please, try again."
msgstr ""

#: Controller/ClientController.php:114
msgid "The client has been deleted."
msgstr ""

#: Controller/ClientController.php:116
msgid "The client could not be deleted. Please, try again."
msgstr ""

#: Controller/EquipoController.php:62;88
msgid "The equipo has been saved."
msgstr ""

#: Controller/EquipoController.php:65;91
msgid "The equipo could not be saved. Please, try again."
msgstr ""

#: Controller/EquipoController.php:111
msgid "The equipo has been deleted."
msgstr ""

#: Controller/EquipoController.php:113
msgid "The equipo could not be deleted. Please, try again."
msgstr ""

#: Controller/LocalController.php:79;105
msgid "The local has been saved."
msgstr ""

#: Controller/LocalController.php:82;108
msgid "The local could not be saved. Please, try again."
msgstr ""

#: Controller/LocalController.php:128
msgid "The local has been deleted."
msgstr ""

#: Controller/LocalController.php:130
msgid "The local could not be deleted. Please, try again."
msgstr ""

#: Controller/ModuleController.php:67;93
msgid "The module has been saved."
msgstr ""

#: Controller/ModuleController.php:70;96
msgid "The module could not be saved. Please, try again."
msgstr ""

#: Controller/ModuleController.php:116
msgid "The module has been deleted."
msgstr ""

#: Controller/ModuleController.php:118
msgid "The module could not be deleted. Please, try again."
msgstr ""

#: Controller/ModuleLocalController.php:71;98
msgid "The module local has been saved."
msgstr ""

#: Controller/ModuleLocalController.php:74;101
msgid "The module local could not be saved. Please, try again."
msgstr ""

#: Controller/ModuleLocalController.php:122
msgid "The module local has been deleted."
msgstr ""

#: Controller/ModuleLocalController.php:124
msgid "The module local could not be deleted. Please, try again."
msgstr ""

#: Controller/ProviderController.php:61;88
msgid "The provider has been saved."
msgstr ""

#: Controller/ProviderController.php:64;91
msgid "The provider could not be saved. Please, try again."
msgstr ""

#: Controller/ProviderController.php:112
msgid "The provider has been deleted."
msgstr ""

#: Controller/ProviderController.php:114
msgid "The provider could not be deleted. Please, try again."
msgstr ""

#: Controller/UsersController.php:76;103
msgid "The user has been saved."
msgstr ""

#: Controller/UsersController.php:79;106
msgid "The user could not be saved. Please, try again."
msgstr ""

#: Controller/UsersController.php:127
msgid "The user has been deleted."
msgstr ""

#: Controller/UsersController.php:129
msgid "The user could not be deleted. Please, try again."
msgstr ""

#: Controller/UsersController.php:143
msgid "Invalid username or password, try again"
msgstr ""

#: Controller/ZonaController.php:55;81
msgid "The zona has been saved."
msgstr ""

#: Controller/ZonaController.php:58;84
msgid "The zona could not be saved. Please, try again."
msgstr ""

#: Controller/ZonaController.php:104
msgid "The zona has been deleted."
msgstr ""

#: Controller/ZonaController.php:106
msgid "The zona could not be deleted. Please, try again."
msgstr ""

#: Template/Client/add.ctp:2
#: Template/Client/edit.ctp:2
#: Template/Client/index.ctp:2;21
#: Template/Client/view.ctp:2;55;94
#: Template/Equipo/add.ctp:2
#: Template/Equipo/edit.ctp:2
#: Template/Equipo/index.ctp:2;20
#: Template/Equipo/view.ctp:2
#: Template/Local/add.ctp:2
#: Template/Local/edit.ctp:2
#: Template/Local/index.ctp:2;19
#: Template/Local/view.ctp:2;58
#: Template/Module/add.ctp:2
#: Template/Module/edit.ctp:2
#: Template/Module/index.ctp:2;17
#: Template/Module/view.ctp:2;43
#: Template/ModuleLocal/add.ctp:2
#: Template/ModuleLocal/edit.ctp:2
#: Template/ModuleLocal/index.ctp:2;20
#: Template/ModuleLocal/view.ctp:2
#: Template/Provider/add.ctp:2
#: Template/Provider/edit.ctp:2
#: Template/Provider/index.ctp:2;22
#: Template/Provider/view.ctp:2
#: Template/Users/add.ctp:2
#: Template/Users/edit.ctp:2
#: Template/Users/index.ctp:2;20
#: Template/Users/view.ctp:2
#: Template/Zona/add.ctp:2
#: Template/Zona/edit.ctp:2
#: Template/Zona/index.ctp:2;18
#: Template/Zona/view.ctp:2
msgid "Actions"
msgstr ""

#: Template/Client/add.ctp:4
#: Template/Client/edit.ctp:10
#: Template/Client/view.ctp:6
#: Template/Local/add.ctp:5
#: Template/Local/edit.ctp:11
#: Template/Local/index.ctp:5
#: Template/Local/view.ctp:8
#: Template/Provider/add.ctp:5
#: Template/Provider/edit.ctp:11
#: Template/Provider/index.ctp:5
#: Template/Provider/view.ctp:8
#: Template/Users/add.ctp:5
#: Template/Users/edit.ctp:11
#: Template/Users/index.ctp:5
#: Template/Users/view.ctp:8
msgid "List Client"
msgstr ""

#: Template/Client/add.ctp:5
#: Template/Client/edit.ctp:11
#: Template/Client/index.ctp:5
#: Template/Client/view.ctp:8
#: Template/Equipo/add.ctp:5
#: Template/Equipo/edit.ctp:11
#: Template/Equipo/index.ctp:5
#: Template/Equipo/view.ctp:8
#: Template/Local/add.ctp:4
#: Template/Local/edit.ctp:10
#: Template/Local/view.ctp:6
#: Template/Module/add.ctp:5
#: Template/Module/edit.ctp:11
#: Template/Module/index.ctp:5
#: Template/Module/view.ctp:8
#: Template/ModuleLocal/add.ctp:7
#: Template/ModuleLocal/edit.ctp:13
#: Template/ModuleLocal/index.ctp:7
#: Template/ModuleLocal/view.ctp:10
#: Template/Provider/add.ctp:7
#: Template/Provider/edit.ctp:13
#: Template/Provider/index.ctp:7
#: Template/Provider/view.ctp:10
#: Template/Users/add.ctp:7
#: Template/Users/edit.ctp:13
#: Template/Users/index.ctp:7
#: Template/Users/view.ctp:10
#: Template/Zona/add.ctp:5
#: Template/Zona/edit.ctp:11
#: Template/Zona/index.ctp:5
#: Template/Zona/view.ctp:8
msgid "List Local"
msgstr ""

#: Template/Client/add.ctp:6
#: Template/Client/edit.ctp:12
#: Template/Client/index.ctp:6
#: Template/Client/view.ctp:9
#: Template/Equipo/add.ctp:6
#: Template/Equipo/edit.ctp:12
#: Template/Equipo/index.ctp:6
#: Template/Equipo/view.ctp:9
#: Template/Local/index.ctp:4
#: Template/Local/view.ctp:7
#: Template/Module/add.ctp:6
#: Template/Module/edit.ctp:12
#: Template/Module/index.ctp:6
#: Template/Module/view.ctp:9
#: Template/ModuleLocal/add.ctp:8
#: Template/ModuleLocal/edit.ctp:14
#: Template/ModuleLocal/index.ctp:8
#: Template/ModuleLocal/view.ctp:11
#: Template/Provider/add.ctp:8
#: Template/Provider/edit.ctp:14
#: Template/Provider/index.ctp:8
#: Template/Provider/view.ctp:11
#: Template/Users/add.ctp:8
#: Template/Users/edit.ctp:14
#: Template/Users/index.ctp:8
#: Template/Users/view.ctp:11
#: Template/Zona/add.ctp:6
#: Template/Zona/edit.ctp:12
#: Template/Zona/index.ctp:6
#: Template/Zona/view.ctp:9
msgid "New Local"
msgstr ""

#: Template/Client/add.ctp:7
#: Template/Client/edit.ctp:13
#: Template/Client/index.ctp:7
#: Template/Client/view.ctp:10
#: Template/Local/add.ctp:7
#: Template/Local/edit.ctp:13
#: Template/Local/index.ctp:7
#: Template/Local/view.ctp:10
#: Template/Provider/add.ctp:4
#: Template/Provider/edit.ctp:10
#: Template/Provider/view.ctp:6
msgid "List Provider"
msgstr ""

#: Template/Client/add.ctp:8
#: Template/Client/edit.ctp:14
#: Template/Client/index.ctp:8
#: Template/Client/view.ctp:11
#: Template/Local/add.ctp:8
#: Template/Local/edit.ctp:14
#: Template/Local/index.ctp:8
#: Template/Local/view.ctp:11
#: Template/Provider/index.ctp:4
#: Template/Provider/view.ctp:7
msgid "New Provider"
msgstr ""

#: Template/Client/add.ctp:14
msgid "Add Client"
msgstr ""

#: Template/Client/add.ctp:23
#: Template/Client/edit.ctp:29
#: Template/Equipo/add.ctp:21
#: Template/Equipo/edit.ctp:27
#: Template/Local/add.ctp:21
#: Template/Local/edit.ctp:27
#: Template/Module/add.ctp:18
#: Template/Module/edit.ctp:24
#: Template/ModuleLocal/add.ctp:20
#: Template/ModuleLocal/edit.ctp:26
#: Template/Provider/add.ctp:26
#: Template/Provider/edit.ctp:32
#: Template/Users/add.ctp:22
#: Template/Users/edit.ctp:28
#: Template/Zona/add.ctp:18
#: Template/Zona/edit.ctp:24
msgid "Submit"
msgstr ""

#: Template/Client/edit.ctp:5
#: Template/Client/index.ctp:36
#: Template/Client/view.ctp:71;111
#: Template/Equipo/edit.ctp:5
#: Template/Equipo/index.ctp:38
#: Template/Local/edit.ctp:5
#: Template/Local/index.ctp:34
#: Template/Local/view.ctp:75
#: Template/Module/edit.ctp:5
#: Template/Module/index.ctp:30
#: Template/Module/view.ctp:59
#: Template/ModuleLocal/edit.ctp:5
#: Template/ModuleLocal/index.ctp:38
#: Template/Provider/edit.ctp:5
#: Template/Provider/index.ctp:42
#: Template/Users/edit.ctp:5
#: Template/Users/index.ctp:38
#: Template/Zona/edit.ctp:5
#: Template/Zona/index.ctp:34
msgid "Delete"
msgstr ""

#: Template/Client/edit.ctp:7
#: Template/Client/index.ctp:36
#: Template/Client/view.ctp:5;71;111
#: Template/Equipo/edit.ctp:7
#: Template/Equipo/index.ctp:38
#: Template/Equipo/view.ctp:5
#: Template/Local/edit.ctp:7
#: Template/Local/index.ctp:34
#: Template/Local/view.ctp:5;75
#: Template/Module/edit.ctp:7
#: Template/Module/index.ctp:30
#: Template/Module/view.ctp:5;59
#: Template/ModuleLocal/edit.ctp:7
#: Template/ModuleLocal/index.ctp:38
#: Template/ModuleLocal/view.ctp:5
#: Template/Provider/edit.ctp:7
#: Template/Provider/index.ctp:42
#: Template/Provider/view.ctp:5
#: Template/Users/edit.ctp:7
#: Template/Users/index.ctp:38
#: Template/Users/view.ctp:5
#: Template/Zona/edit.ctp:7
#: Template/Zona/index.ctp:34
#: Template/Zona/view.ctp:5
msgid "Are you sure you want to delete # {0}?"
msgstr ""

#: Template/Client/edit.ctp:20
#: Template/Client/view.ctp:4
msgid "Edit Client"
msgstr ""

#: Template/Client/index.ctp:4
#: Template/Client/view.ctp:7
#: Template/Local/add.ctp:6
#: Template/Local/edit.ctp:12
#: Template/Local/index.ctp:6
#: Template/Local/view.ctp:9
#: Template/Provider/add.ctp:6
#: Template/Provider/edit.ctp:12
#: Template/Provider/index.ctp:6
#: Template/Provider/view.ctp:9
#: Template/Users/add.ctp:6
#: Template/Users/edit.ctp:12
#: Template/Users/index.ctp:6
#: Template/Users/view.ctp:9
msgid "New Client"
msgstr ""

#: Template/Client/index.ctp:34
#: Template/Client/view.ctp:67;107
#: Template/Equipo/index.ctp:36
#: Template/Local/index.ctp:32
#: Template/Local/view.ctp:71
#: Template/Module/index.ctp:28
#: Template/Module/view.ctp:55
#: Template/ModuleLocal/index.ctp:36
#: Template/Provider/index.ctp:40
#: Template/Users/index.ctp:36
#: Template/Zona/index.ctp:32
msgid "View"
msgstr ""

#: Template/Client/index.ctp:35
#: Template/Client/view.ctp:69;109
#: Template/Equipo/index.ctp:37
#: Template/Local/index.ctp:33
#: Template/Local/view.ctp:73
#: Template/Module/index.ctp:29
#: Template/Module/view.ctp:57
#: Template/ModuleLocal/index.ctp:37
#: Template/Provider/index.ctp:41
#: Template/Users/index.ctp:37
#: Template/Zona/index.ctp:33
msgid "Edit"
msgstr ""

#: Template/Client/index.ctp:45
#: Template/Equipo/index.ctp:47
#: Template/Local/index.ctp:42
#: Template/Module/index.ctp:39
#: Template/ModuleLocal/index.ctp:47
#: Template/Provider/index.ctp:51
#: Template/Users/index.ctp:47
#: Template/Zona/index.ctp:43
msgid "previous"
msgstr ""

#: Template/Client/index.ctp:47
#: Template/Equipo/index.ctp:49
#: Template/Local/index.ctp:44
#: Template/Module/index.ctp:41
#: Template/ModuleLocal/index.ctp:49
#: Template/Provider/index.ctp:53
#: Template/Users/index.ctp:49
#: Template/Zona/index.ctp:45
msgid "next"
msgstr ""

#: Template/Client/view.ctp:5
msgid "Delete Client"
msgstr ""

#: Template/Client/view.ctp:18;51;90
#: Template/Equipo/view.ctp:18
#: Template/Local/view.ctp:34;54
#: Template/Module/view.ctp:16;39
#: Template/Provider/view.ctp:22
#: Template/Zona/view.ctp:18
msgid "Name"
msgstr ""

#: Template/Client/view.ctp:20
msgid "Cif"
msgstr ""

#: Template/Client/view.ctp:22
msgid "Email"
msgstr ""

#: Template/Client/view.ctp:24
msgid "Phone"
msgstr ""

#: Template/Client/view.ctp:28;49;87
#: Template/Equipo/view.ctp:22
#: Template/Local/view.ctp:22;51
#: Template/Module/view.ctp:20;37
#: Template/ModuleLocal/view.ctp:24
#: Template/Provider/view.ctp:26
#: Template/Users/view.ctp:28
#: Template/Zona/view.ctp:22
msgid "Id"
msgstr ""

#: Template/Client/view.ctp:32;53;92
#: Template/Equipo/view.ctp:32
#: Template/Local/view.ctp:26;56
#: Template/Module/view.ctp:24;41
#: Template/ModuleLocal/view.ctp:28
#: Template/Provider/view.ctp:30
#: Template/Users/view.ctp:32
#: Template/Zona/view.ctp:26
msgid "Created"
msgstr ""

#: Template/Client/view.ctp:34;54;93
#: Template/Equipo/view.ctp:34
#: Template/Local/view.ctp:28;57
#: Template/Module/view.ctp:26;42
#: Template/ModuleLocal/view.ctp:30
#: Template/Provider/view.ctp:32
#: Template/Users/view.ctp:34
#: Template/Zona/view.ctp:28
msgid "Modified"
msgstr ""

#: Template/Client/view.ctp:38
msgid "Enabled"
msgstr ""

#: Template/Client/view.ctp:39
#: Template/Provider/view.ctp:37;39;41;43
msgid "Yes"
msgstr ""

#: Template/Client/view.ctp:39
#: Template/Provider/view.ctp:37;39;41;43
msgid "No"
msgstr ""

#: Template/Client/view.ctp:45
#: Template/Module/view.ctp:33
msgid "Related Local"
msgstr ""

#: Template/Client/view.ctp:50;88
#: Template/Local/view.ctp:52
#: Template/Module/view.ctp:38
msgid "Client Id"
msgstr ""

#: Template/Client/view.ctp:52
#: Template/Local/view.ctp:40
#: Template/Module/view.ctp:40
msgid "Address"
msgstr ""

#: Template/Client/view.ctp:83
#: Template/Local/view.ctp:47
msgid "Related Provider"
msgstr ""

#: Template/Client/view.ctp:89
#: Template/Local/view.ctp:53
msgid "Local Id"
msgstr ""

#: Template/Client/view.ctp:91
#: Template/Local/view.ctp:55
#: Template/Provider/view.ctp:48
msgid "Description"
msgstr ""

#: Template/Equipo/add.ctp:4
#: Template/Equipo/edit.ctp:10
#: Template/Equipo/view.ctp:6
msgid "List Equipo"
msgstr ""

#: Template/Equipo/add.ctp:12
msgid "Add Equipo"
msgstr ""

#: Template/Equipo/edit.ctp:18
#: Template/Equipo/view.ctp:4
msgid "Edit Equipo"
msgstr ""

#: Template/Equipo/index.ctp:4
#: Template/Equipo/view.ctp:7
msgid "New Equipo"
msgstr ""

#: Template/Equipo/view.ctp:5
msgid "Delete Equipo"
msgstr ""

#: Template/Equipo/view.ctp:16
#: Template/ModuleLocal/view.ctp:20
#: Template/Provider/view.ctp:20
#: Template/Users/view.ctp:20
#: Template/Zona/view.ctp:16
msgid "Local"
msgstr ""

#: Template/Equipo/view.ctp:24
msgid "Max"
msgstr ""

#: Template/Equipo/view.ctp:26
msgid "Min"
msgstr ""

#: Template/Equipo/view.ctp:28
msgid "Standard"
msgstr ""

#: Template/Local/add.ctp:14
msgid "Add Local"
msgstr ""

#: Template/Local/edit.ctp:20
#: Template/Local/view.ctp:4
msgid "Edit Local"
msgstr ""

#: Template/Local/view.ctp:5
msgid "Delete Local"
msgstr ""

#: Template/Local/view.ctp:18
#: Template/Provider/view.ctp:18
#: Template/Users/view.ctp:18
msgid "Client"
msgstr ""

#: Template/Module/add.ctp:4
#: Template/Module/edit.ctp:10
#: Template/Module/view.ctp:6
#: Template/ModuleLocal/add.ctp:5
#: Template/ModuleLocal/edit.ctp:11
#: Template/ModuleLocal/index.ctp:5
#: Template/ModuleLocal/view.ctp:8
msgid "List Module"
msgstr ""

#: Template/Module/add.ctp:12
msgid "Add Module"
msgstr ""

#: Template/Module/edit.ctp:18
#: Template/Module/view.ctp:4
msgid "Edit Module"
msgstr ""

#: Template/Module/index.ctp:4
#: Template/Module/view.ctp:7
#: Template/ModuleLocal/add.ctp:6
#: Template/ModuleLocal/edit.ctp:12
#: Template/ModuleLocal/index.ctp:6
#: Template/ModuleLocal/view.ctp:9
msgid "New Module"
msgstr ""

#: Template/Module/view.ctp:5
msgid "Delete Module"
msgstr ""

#: Template/ModuleLocal/add.ctp:4
#: Template/ModuleLocal/edit.ctp:10
#: Template/ModuleLocal/view.ctp:6
msgid "List Module Local"
msgstr ""

#: Template/ModuleLocal/add.ctp:14
msgid "Add Module Local"
msgstr ""

#: Template/ModuleLocal/edit.ctp:20
#: Template/ModuleLocal/view.ctp:4
msgid "Edit Module Local"
msgstr ""

#: Template/ModuleLocal/index.ctp:4
#: Template/ModuleLocal/view.ctp:7
msgid "New Module Local"
msgstr ""

#: Template/ModuleLocal/view.ctp:5
msgid "Delete Module Local"
msgstr ""

#: Template/ModuleLocal/view.ctp:18
msgid "Module"
msgstr ""

#: Template/Pages/index.ctp:5
msgid "Clientes"
msgstr ""

#: Template/Pages/index.ctp:7;15;23;31
msgid "Visualizar"
msgstr ""

#: Template/Pages/index.ctp:8;16;24;32
msgid "Insertar"
msgstr ""

#: Template/Pages/index.ctp:13
msgid "Locales"
msgstr ""

#: Template/Pages/index.ctp:21
msgid "Usuarios"
msgstr ""

#: Template/Pages/index.ctp:29
msgid "Proveedores"
msgstr ""

#: Template/Provider/add.ctp:14
msgid "Add Provider"
msgstr ""

#: Template/Provider/edit.ctp:20
#: Template/Provider/view.ctp:4
msgid "Edit Provider"
msgstr ""

#: Template/Provider/view.ctp:5
msgid "Delete Provider"
msgstr ""

#: Template/Provider/view.ctp:36
msgid "Congelados"
msgstr ""

#: Template/Provider/view.ctp:38
msgid "Carne"
msgstr ""

#: Template/Provider/view.ctp:40
msgid "Pescado"
msgstr ""

#: Template/Provider/view.ctp:42
msgid "Fruta"
msgstr ""

#: Template/Users/add.ctp:4
#: Template/Users/edit.ctp:10
#: Template/Users/view.ctp:6
msgid "List Users"
msgstr ""

#: Template/Users/add.ctp:14
msgid "Add User"
msgstr ""

#: Template/Users/edit.ctp:20
#: Template/Users/view.ctp:4
msgid "Edit User"
msgstr ""

#: Template/Users/index.ctp:4
#: Template/Users/view.ctp:7
msgid "New User"
msgstr ""

#: Template/Users/login.ctp:5
msgid "Please enter your username and password"
msgstr ""

#: Template/Users/login.ctp:9
msgid "Login"
msgstr ""

#: Template/Users/view.ctp:5
msgid "Delete User"
msgstr ""

#: Template/Users/view.ctp:22
msgid "Username"
msgstr ""

#: Template/Users/view.ctp:24
msgid "Password"
msgstr ""

#: Template/Zona/add.ctp:4
#: Template/Zona/edit.ctp:10
#: Template/Zona/view.ctp:6
msgid "List Zona"
msgstr ""

#: Template/Zona/add.ctp:12
msgid "Add Zona"
msgstr ""

#: Template/Zona/edit.ctp:18
#: Template/Zona/view.ctp:4
msgid "Edit Zona"
msgstr ""

#: Template/Zona/index.ctp:4
#: Template/Zona/view.ctp:7
msgid "New Zona"
msgstr ""

#: Template/Zona/view.ctp:5
msgid "Delete Zona"
msgstr ""

